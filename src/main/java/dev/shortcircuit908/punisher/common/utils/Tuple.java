package dev.shortcircuit908.punisher.common.utils;

import java.util.Arrays;
import java.util.Iterator;

/**
 * @author ShortCircuit908
 * Created on 3/28/2018.
 */
public class Tuple<T> implements Iterable<T> {
	private final T[] elements;
	
	@SafeVarargs
	public Tuple(T... elements) {
		this.elements = elements;
	}
	
	@SuppressWarnings("unchecked")
	public <E extends T> E get(int index) throws ClassCastException {
		if (index >= elements.length) {
			return null;
		}
		return (E) elements[index];
	}
	
	@Override
	public Iterator<T> iterator() {
		return Arrays.stream(elements).iterator();
	}
	
	public int size() {
		return elements.length;
	}
}
