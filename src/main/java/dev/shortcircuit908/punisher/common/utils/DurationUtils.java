package dev.shortcircuit908.punisher.common.utils;

import java.time.Duration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author ShortCircuit908
 * Created on 3/19/2018.
 */
public class DurationUtils {
	public static Duration fromString(String str) {
		Long seconds = null;
		Long minutes = null;
		Long hours = null;
		Long days = null;
		Long weeks = null;
		Long years = null;
		Pattern pattern = Pattern.compile("^[0-9]+[smhdwy]", Pattern.CASE_INSENSITIVE);
		while (str.length() > 0) {
			Matcher matcher = pattern.matcher(str);
			if (!matcher.find()) {
				throw new IllegalArgumentException("Unable to parse string: " + str);
			}
			String group = matcher.group();
			char suffix = Character.toLowerCase(group.charAt(group.length() - 1));
			long num = Long.parseLong(group.substring(0, group.length() - 1));
			switch (suffix) {
				case 's':
					seconds = num;
					break;
				case 'm':
					minutes = num;
					break;
				case 'h':
					hours = num;
					break;
				case 'd':
					days = num;
					break;
				case 'w':
					weeks = num;
					break;
				case 'y':
					years = num;
					break;
			}
			str = str.substring(matcher.end());
		}
		
		return Duration.ofSeconds(seconds == null ? 0 : seconds)
				.plusMinutes(minutes == null ? 0 : minutes)
				.plusHours(hours == null ? 0 : hours)
				.plusDays(days == null ? 0 : days)
				.plusDays(weeks == null ? 0 : weeks * 7)
				.plusDays(years == null ? 0 : years * 365);
	}
	
	public static String toString(Duration duration) {
		if (duration == null) {
			return "";
		}
		StringBuilder builder = new StringBuilder();
		long seconds = duration.getSeconds();
		if (seconds < 0) {
			seconds *= -1;
			builder.append('-');
		}
		long years = seconds / (60 * 60 * 24 * 365);
		seconds -= years * (60 * 60 * 24 * 365);
		long days = seconds / (60 * 60 * 24);
		seconds -= days * (60 * 60 * 24);
		long hours = seconds / (60 * 60);
		seconds -= hours * (60 * 60);
		long minutes = seconds / 60;
		seconds -= minutes * 60;
		long weeks = days / 7;
		days -= weeks * 7;
		if (years > 0) {
			builder.append(years).append("y ");
		}
		if (weeks > 0) {
			builder.append(weeks).append("w ");
		}
		if (days > 0) {
			builder.append(days).append("d ");
		}
		if (hours > 0) {
			builder.append(hours).append("h ");
		}
		if (minutes > 0) {
			builder.append(minutes).append("m ");
		}
		if (seconds > 0 || builder.length() == 0) {
			builder.append(seconds).append('s');
		}
		return builder.toString().trim();
	}
}
