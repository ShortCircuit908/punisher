package dev.shortcircuit908.punisher.common.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author ShortCircuit908
 * Created on 3/25/2018.
 */
public class GenericUtils {
	public static Class<?>[] analyzeElementTypes(Iterable<?> set) {
		List<Class<?>> common_classes = new ArrayList<>(8);
		for (Object obj : set) {
			if (obj != null) {
				List<Class<?>> current_classes = getFullClassHierarchy(obj.getClass());
				current_classes.removeIf(common_classes::contains);
				common_classes.addAll(current_classes);
			}
		}
		for (Object obj : set) {
			if (obj != null) {
				List<Class<?>> current_classes = getFullClassHierarchy(obj.getClass());
				common_classes.removeIf(common_class -> !current_classes.contains(common_class));
			}
		}
		return common_classes.toArray(new Class[0]);
	}
	
	public static List<Class<?>> getFullClassHierarchy(Class<?> clazz) {
		List<Class<?>> current_classes = new ArrayList<>(8);
		do {
			current_classes.add(clazz);
			Class<?>[] interfaces = clazz.getInterfaces();
			Collections.addAll(current_classes, interfaces);
			clazz = clazz.getSuperclass();
		}
		while (clazz != null);
		return current_classes;
	}
}
