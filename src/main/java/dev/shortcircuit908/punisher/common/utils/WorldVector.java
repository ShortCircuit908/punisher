package dev.shortcircuit908.punisher.common.utils;

import java.util.Objects;

/**
 * @author ShortCircuit908
 * Created on 6/8/2018.
 */
public class WorldVector extends Vector3d {
	public String world;
	public float pitch;
	public float yaw;
	
	public WorldVector(String world, double x, double y, double z) {
		this(world, x, y, z, 0.0f, 0.0f);
	}
	
	
	public WorldVector(String world, double x, double y, double z, float pitch, float yaw) {
		super(x, y, z);
		this.world = world;
		this.pitch = pitch;
		this.yaw = yaw;
	}
	
	public static WorldVector fromString(String string) {
		String[] parts = string.split(";");
		String[] loc_parts = parts[1].split(",");
		return new WorldVector(parts[0], Double.parseDouble(loc_parts[0]), Double.parseDouble(loc_parts[1]),
				Double.parseDouble(loc_parts[2]));
	}
	
	public String getWorldName() {
		return world;
	}
	
	@Override
	public WorldVector add(Vector3 other) {
		return new WorldVector(world, x + other.x.doubleValue(), y + other.y.doubleValue(), z + other.z.doubleValue());
	}
	
	@Override
	public WorldVector subtract(Vector3 other) {
		return new WorldVector(world, x - other.x.doubleValue(), y - other.y.doubleValue(), z - other.z.doubleValue());
	}
	
	@Override
	public WorldVector multiply(Vector3 other) {
		return new WorldVector(world, x * other.x.doubleValue(), y * other.y.doubleValue(), z * other.z.doubleValue());
	}
	
	@Override
	public WorldVector multiply(int m) {
		return new WorldVector(world, x * m, y * m, z * m);
	}
	
	@Override
	public WorldVector multiply(double m) {
		return new WorldVector(world, x * m, y * m, z * m);
	}
	
	@Override
	public WorldVector divide(Vector3 other) {
		return new WorldVector(world, x / other.x.doubleValue(), y / other.y.doubleValue(), z / other.z.doubleValue());
	}
	
	@Override
	public double distanceSquared(Vector3 other) {
		String other_world = null;
		if (!(other instanceof WorldVector) || Objects.equals(other_world = ((WorldVector) other).world, world)) {
			throw new IllegalArgumentException("Cannot measure distance between " + world + " and " + other_world);
		}
		return super.distanceSquared(other);
	}
	
	@Override
	public int hashCode() {
		int hash = 3;
		hash = 19 * hash + (this.world != null ? this.world.hashCode() : 0);
		hash = 19 * hash + (int) (Double.doubleToLongBits(this.x) ^ (Double.doubleToLongBits(this.x) >>> 32));
		hash = 19 * hash + (int) (Double.doubleToLongBits(this.y) ^ (Double.doubleToLongBits(this.y) >>> 32));
		hash = 19 * hash + (int) (Double.doubleToLongBits(this.z) ^ (Double.doubleToLongBits(this.z) >>> 32));
		hash = 19 * hash + Float.floatToIntBits(this.pitch);
		hash = 19 * hash + Float.floatToIntBits(this.yaw);
		return hash;
	}
	
	@Override
	public String toString() {
		return world + ";" + super.toString();
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof WorldVector) || ((WorldVector) o).getWorldName().equals(getWorldName())) {
			return false;
		}
		return super.equals(o);
	}
}
