package dev.shortcircuit908.punisher.common.utils;

/**
 * @author ShortCircuit908
 * Created on 4/23/2018.
 */
public abstract class Vector3<T extends Number> implements Cloneable {
	private static final double epsilon = 0.000001;
	public T x;
	public T y;
	public T z;
	
	public Vector3(T x, T y, T z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public static double getEpsilon() {
		return epsilon;
	}
	
	public abstract Vector3<T> add(Vector3 other);
	
	public abstract Vector3<T> add(Number x, Number y, Number z);
	
	public abstract Vector3<T> subtract(Vector3 other);
	
	public abstract Vector3<T> subtract(Number x, Number y, Number z);
	
	public abstract Vector3<T> multiply(Vector3 other);
	
	public abstract Vector3<T> multiply(Number x, Number y, Number z);
	
	public abstract Vector3<T> multiply(int m);
	
	public abstract Vector3<T> multiply(double m);
	
	public abstract Vector3<T> divide(Vector3 other);
	
	public abstract Vector3<T> divide(Number x, Number y, Number z);
	
	public double lengthSquared() {
		return Math.pow(x.doubleValue(), 2) + Math.pow(y.doubleValue(), 2) + Math.pow(z.doubleValue(), 2);
	}
	
	public double length() {
		return Math.sqrt(lengthSquared());
	}
	
	public double distanceSquared(Vector3 other) {
		return Math.pow(other.x.doubleValue() - x.doubleValue(), 2)
				+ Math.pow(other.y.doubleValue() - y.doubleValue(), 2)
				+ Math.pow(other.z.doubleValue() - z.doubleValue(), 2);
	}
	
	public double distance(Vector3 other) {
		return Math.sqrt(distanceSquared(other));
	}
	
	public Vector3d normalize() {
		double length =
				Math.sqrt(Math.pow(x.doubleValue(), 2) + Math.pow(y.doubleValue(), 2) + Math.pow(z.doubleValue(), 2));
		return new Vector3d(x.doubleValue() / length, y.doubleValue() / length, z.doubleValue() / length);
	}
	
	@Override
	public String toString() {
		return x + "," + y + "," + z;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Vector3<T> clone() {
		try {
			return (Vector3<T>) super.clone();
		}
		catch (CloneNotSupportedException e) {
			throw new Error(e);
		}
	}
	
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Vector3)) {
			return false;
		}
		return Math.abs(x.doubleValue() - ((Vector3) other).x.doubleValue()) < epsilon
				&& Math.abs(y.doubleValue() - ((Vector3) other).y.doubleValue()) < epsilon
				&& Math.abs(z.doubleValue() - ((Vector3) other).z.doubleValue()) < epsilon;
	}
	
	@Override
	public int hashCode() {
		int hash = 7;
		hash = 79 * hash + (int) (Double.doubleToLongBits(this.x.doubleValue()) ^
				(Double.doubleToLongBits(this.x.doubleValue()) >>> 32));
		hash = 79 * hash + (int) (Double.doubleToLongBits(this.y.doubleValue()) ^
				(Double.doubleToLongBits(this.y.doubleValue()) >>> 32));
		hash = 79 * hash + (int) (Double.doubleToLongBits(this.z.doubleValue()) ^
				(Double.doubleToLongBits(this.z.doubleValue()) >>> 32));
		return hash;
	}
}
