package dev.shortcircuit908.punisher.common.utils;

import com.google.common.collect.ImmutableSet;
import dev.shortcircuit908.punisher.common.PunisherDataSource;

import javax.sql.DataSource;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author ShortCircuit908
 * Created on 4/17/2018.
 */
public class StoredPlayerUtils {
	private static final List<PlayerInfo> player_cache = new ArrayList<>();
	
	public static void init() throws SQLException {
		loadAllPlayers();
	}
	
	public static synchronized void loadAllPlayers() throws SQLException {
		Optional<DataSource> source_opt = PunisherDataSource.getPrimaryDataSource();
		if (!source_opt.isPresent()) {
			throw new SQLException("Database not initialized!");
		}
		DataSource source = source_opt.get();
		try (Connection connection = source.getConnection();
			 PreparedStatement statement =
					 connection.prepareStatement(
							 "SELECT `a`.`id`, `a`.`player_id`, `a`.`username`, `a`.`last_online`, `b`.`address` " +
									 "FROM `punisher_uuid_map` AS `a` LEFT JOIN `punisher_ip_address_book` AS `b` ON " +
									 "`a`.`id`=`b`.`player_id`");
			 ResultSet result = statement.executeQuery()) {
			player_cache.clear();
			while (result.next()) {
				Integer database_id = result.getInt(1);
				String raw_uuid = result.getString(2);
				final UUID uuid = raw_uuid == null ? null : UUID.fromString(raw_uuid);
				final String username = result.getString(3);
				Date raw_date = result.getDate(4);
				final Instant last_online = raw_date == null ? null : raw_date.toInstant();
				String raw_address = result.getString(5);
				final InetAddress address = raw_address == null ? null : IPUtils.parseInetAddres(raw_address);
				Optional<PlayerInfo> existing_entry_opt =
						player_cache.stream()
								.filter(check_info -> Objects.equals(database_id, check_info.database_id))
								.findFirst();
				if (existing_entry_opt.isPresent()) {
					existing_entry_opt.get().addresses.add(address);
				}
				else {
					HashSet<InetAddress> addresses = new HashSet<>();
					addresses.add(address);
					player_cache.add(new PlayerInfo(database_id, uuid, username, last_online, addresses));
				}
			}
		}
	}
	
	public static synchronized void flushAllPlayers() throws SQLException {
		Optional<DataSource> source_opt = PunisherDataSource.getPrimaryDataSource();
		if (!source_opt.isPresent()) {
			throw new SQLException("Database not initialized!");
		}
		DataSource source = source_opt.get();
		try (Connection connection = source.getConnection()) {
			boolean autocommit = connection.getAutoCommit();
			connection.setAutoCommit(false);
			final int insert_row_limit = 1000;
			
			Stream<PlayerInfo> modified_values = player_cache.stream()
					.filter(PlayerInfo::isModified);
			List<PlayerInfo> all_modified_values = modified_values.collect(Collectors.toList());
			List<PlayerInfo> values_with_ids = modified_values
					.filter(PlayerInfo::hasDatabaseId)
					.collect(Collectors.toList());
			List<PlayerInfo> values_without_ids = modified_values
					.filter(PlayerInfo::notInDatabase)
					.collect(Collectors.toList());
			
			// Insert new records
			if(!values_without_ids.isEmpty()) {
				try (PreparedStatement statement = connection.prepareStatement("INSERT INTO `punisher_uuid_map` " +
								"(`player_id`, `username`, `last_online`) VALUES (?, ?, ?)",
						Statement.RETURN_GENERATED_KEYS)) {
					int insert_count = 0;
					List<Integer> generated_keys = new ArrayList<>(values_without_ids.size());
					for (PlayerInfo player_info : values_without_ids) {
						statement.setString(1, player_info.uuid == null ? null : player_info.uuid.toString());
						statement.setString(2, player_info.username);
						statement.setDate(3, player_info.last_online == null ? null :
								new Date(player_info.last_online.toEpochMilli()));
						statement.addBatch();
						insert_count++;
						if (insert_count % insert_row_limit == 0 || insert_count == values_without_ids.size()) {
							statement.executeBatch();
							try (ResultSet generated_keys_result = statement.getGeneratedKeys()) {
								while (generated_keys_result.next()) {
									generated_keys.add(generated_keys_result.getInt(1));
								}
							}
						}
					}
					for (int i = 0; i < generated_keys.size(); i++) {
						values_without_ids.get(i).database_id = generated_keys.get(i);
					}
				}
			}
			
			// Update existing records
			if(!values_with_ids.isEmpty()) {
				try (PreparedStatement statement = connection.prepareStatement("UPDATE `punisher_uuid_map` SET " +
						"`player_id`=?, `username`=?, `last_online`=? WHERE `id`=?")) {
					int insert_count = 0;
					for (PlayerInfo player_info : values_with_ids) {
						statement.setString(1, player_info.uuid == null ? null : player_info.uuid.toString());
						statement.setString(2, player_info.username);
						statement.setDate(3, player_info.last_online == null ? null :
								new Date(player_info.last_online.toEpochMilli()));
						statement.setInt(4, player_info.database_id);
						statement.addBatch();
						insert_count++;
						if (insert_count % insert_row_limit == 0 || insert_count == values_without_ids.size()) {
							statement.executeBatch();
						}
					}
				}
			}
			
			// Update known IP addresses
			if(!all_modified_values.isEmpty()) {
				try (PreparedStatement statement = connection.prepareStatement(
						"INSERT INTO `punisher_ip_address_book` " +
								"(`player_id`, `address`) VALUES (?, ?) ON DUPLICATE KEY UPDATE `id`=`id`")) {
					int insert_count = 0;
					for (PlayerInfo player_info : player_cache) {
						for (InetAddress address : player_info.addresses) {
							statement.setInt(1, player_info.database_id);
							statement.setString(2, address.getHostAddress());
							statement.addBatch();
							insert_count++;
							if (insert_count % insert_row_limit == 0 || insert_count == player_cache.size()) {
								statement.executeBatch();
							}
						}
					}
				}
			}
			
			connection.commit();
			connection.setAutoCommit(autocommit);
		}
		for(PlayerInfo player_info : player_cache){
			player_info.modified = false;
		}
	}
	
	public static synchronized List<PlayerInfo> getExistingPlayer(String username) {
		return player_cache.stream()
				.filter(info -> info.username != null && info.username.toLowerCase().contains(username.toLowerCase()))
				.collect(Collectors.toList());
	}
	
	public static synchronized Optional<PlayerInfo> getExistingPlayer(UUID uuid) {
		if (uuid == null) {
			return Optional.empty();
		}
		return player_cache.stream()
				.filter(info -> uuid.equals(info.uuid))
				.findFirst();
	}
	
	public static synchronized List<PlayerInfo> getExistingPlayer(InetAddress address) {
		return player_cache.stream()
				.filter(info -> info.getAddresses().contains(address))
				.collect(Collectors.toList());
	}
	
	public static synchronized List<PlayerInfo> getOrCreatePlayer(InetAddress address){
		List<PlayerInfo> result = getExistingPlayer(address);
		if(result.isEmpty()){
			HashSet<InetAddress> addresses = new HashSet<>();
			addresses.add(address);
			PlayerInfo new_player = new PlayerInfo(null, null, null, null, addresses);
			player_cache.add(new_player);
			result.add(new_player);
		}
		return result;
	}
	
	public static synchronized List<PlayerInfo> getOrCreatePlayer(String username){
		List<PlayerInfo> result = getExistingPlayer(username);
		if(result.isEmpty()){
			PlayerInfo new_player = new PlayerInfo(null, null, username, null, null);
			player_cache.add(new_player);
			result.add(new_player);
		}
		return result;
	}
	
	public static synchronized PlayerInfo getOrCreatePlayer(UUID uuid){
		return getExistingPlayer(uuid).orElse(new PlayerInfo(null, uuid, null, null, null));
	}
	
	public static class PlayerInfo {
		private Integer database_id;
		private UUID uuid;
		private String username;
		private Instant last_online;
		private final Set<InetAddress> addresses;
		private boolean modified;
		
		private PlayerInfo(Integer database_id, UUID uuid, String username, Instant last_online) {
			this(database_id, uuid, username, last_online, null);
		}
		
		private PlayerInfo(Integer database_id, UUID uuid, String username, Instant last_online,
						   Set<InetAddress> address) {
			this.database_id = database_id;
			this.uuid = uuid;
			this.username = username;
			this.last_online = last_online;
			this.addresses = address == null ? new HashSet<>() : address;
			this.modified = database_id == null;
		}
		
		public boolean notInDatabase() {
			return database_id == null;
		}
		
		public boolean hasDatabaseId() {
			return database_id != null;
		}
		
		public Optional<Integer> getDatabaseId() {
			return Optional.ofNullable(database_id);
		}
		
		public UUID getUniqueId() {
			return uuid;
		}
		
		public void setUniqueId(UUID uuid) {
			modified |= !Objects.equals(this.uuid, uuid);
			this.uuid = uuid;
		}
		
		public Optional<String> getUsername() {
			return Optional.ofNullable(username);
		}
		
		public void setUsername(String username) {
			modified |= !Objects.equals(this.username, username);
			this.username = username;
		}
		
		public Optional<Instant> getLastOnline() {
			return Optional.ofNullable(last_online);
		}
		
		public void setLastOnline(Instant last_online) {
			modified |= !Objects.equals(this.last_online, last_online);
			this.last_online = last_online;
		}
		
		
		public Set<InetAddress> getAddresses() {
			return ImmutableSet.copyOf(addresses);
		}
		
		public boolean addAddress(InetAddress address) {
			return modified |= addresses.add(address);
		}
		
		public boolean removeAddress(InetAddress address) {
			return modified |= addresses.remove(address);
		}
		
		private boolean isModified() {
			return modified;
		}
		
		private void setModified(boolean modified) {
			this.modified = modified;
		}
		
		@Override
		public String toString() {
			return String.format("[dbid=%1$d, uuid=%2$s, name=%3$s, seen=%4$s, addresses=%5$s]",
					database_id,
					uuid,
					username,
					last_online,
					Arrays.toString(addresses.toArray()));
		}
		
		@Override
		public boolean equals(Object o) {
			return o instanceof PlayerInfo
					&& Objects.equals(uuid, ((PlayerInfo) o).uuid)
					&& Objects.equals(username, ((PlayerInfo) o).username)
					&& Objects.equals(last_online, ((PlayerInfo) o).last_online)
					&& Objects.equals(addresses, ((PlayerInfo) o).addresses);
		}
	}
}
