package dev.shortcircuit908.punisher.common.utils;

import org.bukkit.util.Vector;

/**
 * @author ShortCircuit908
 * Created on 4/23/2018.
 */
public class Vector3d extends Vector3<Double> {
	public static final Vector3d ZERO = new Vector3d(0, 0, 0);
	
	public Vector3d(double x, double y, double z) {
		super(x, y, z);
	}
	
	public static Vector3d fromBukkitVector(Vector vector) {
		return new Vector3d(vector.getX(), vector.getY(), vector.getZ());
	}
	
	@Override
	public Vector3d add(Vector3 other) {
		return add(other.x, other.y, other.z);
	}
	
	@Override
	public Vector3d add(Number x, Number y, Number z) {
		return new Vector3d(this.x + x.doubleValue(), this.y + y.doubleValue(), this.z + z.doubleValue());
	}
	
	@Override
	public Vector3d subtract(Vector3 other) {
		return subtract(other.x, other.y, other.z);
	}
	
	@Override
	public Vector3d subtract(Number x, Number y, Number z) {
		return new Vector3d(this.x - x.doubleValue(), this.y - y.doubleValue(), this.z - z.doubleValue());
	}
	
	@Override
	public Vector3d multiply(Vector3 other) {
		return multiply(other.x, other.y, other.z);
	}
	
	@Override
	public Vector3d multiply(Number x, Number y, Number z) {
		return new Vector3d(this.x * x.doubleValue(), this.y * y.doubleValue(), this.z * z.doubleValue());
	}
	
	@Override
	public Vector3d multiply(int m) {
		return new Vector3d(x * m, y * m, z * m);
	}
	
	@Override
	public Vector3d multiply(double m) {
		return new Vector3d(x * m, y * m, z * m);
	}
	
	@Override
	public Vector3d divide(Vector3 other) {
		return divide(other.x, other.y, other.z);
	}
	
	@Override
	public Vector3d divide(Number x, Number y, Number z) {
		return new Vector3d(this.x / x.doubleValue(), this.y / y.doubleValue(), this.z / z.doubleValue());
	}
}
