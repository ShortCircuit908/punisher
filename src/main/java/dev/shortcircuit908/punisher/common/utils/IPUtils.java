package dev.shortcircuit908.punisher.common.utils;

import com.google.common.net.InetAddresses;

import java.net.InetAddress;

/**
 * @author Caleb Milligan
 * Created on 7/19/2018.
 */
public class IPUtils {
	public static InetAddress parseInetAddres(String address) {
		return InetAddresses.forString(address);
	}
}
