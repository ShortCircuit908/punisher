package dev.shortcircuit908.punisher.common.utils;

import org.bukkit.util.Vector;

/**
 * @author ShortCircuit908
 * Created on 4/23/2018.
 */
public class Vector3i extends Vector3<Integer> {
	public static final Vector3i ZERO = new Vector3i(0, 0, 0);
	
	public Vector3i(int x, int y, int z) {
		super(x, y, z);
	}
	
	public static Vector3i fromBukkitVector(Vector vector) {
		return new Vector3i(vector.getBlockX(), vector.getBlockY(), vector.getBlockZ());
	}
	
	public static Vector toBukkitVector(Vector3i vector) {
		return new Vector(vector.x, vector.y, vector.z);
	}
	
	@Override
	public Vector3i add(Vector3 other) {
		return add(other.x, other.y, other.z);
	}
	
	@Override
	public Vector3i add(Number x, Number y, Number z) {
		return new Vector3i(this.x + x.intValue(), this.y + y.intValue(), this.z + z.intValue());
	}
	
	@Override
	public Vector3i subtract(Vector3 other) {
		return subtract(other.x, other.y, other.z);
	}
	
	@Override
	public Vector3i subtract(Number x, Number y, Number z) {
		return new Vector3i(this.x - x.intValue(), this.y - y.intValue(), this.z - z.intValue());
	}
	
	@Override
	public Vector3i multiply(Vector3 other) {
		return multiply(other.x, other.y, other.z);
	}
	
	@Override
	public Vector3i multiply(Number x, Number y, Number z) {
		return new Vector3i(this.x * x.intValue(), this.y * y.intValue(), this.z * z.intValue());
	}
	
	@Override
	public Vector3i multiply(int m) {
		return new Vector3i(x * m, y * m, z * m);
	}
	
	@Override
	public Vector3i multiply(double m) {
		return new Vector3i((int) (x * m), (int) (y * m), (int) (z * m));
	}
	
	@Override
	public Vector3i divide(Vector3 other) {
		return divide(other.x, other.y, other.z);
	}
	
	@Override
	public Vector3i divide(Number x, Number y, Number z) {
		return new Vector3i(this.x / x.intValue(), this.y / y.intValue(), this.z / z.intValue());
	}
}
