package dev.shortcircuit908.punisher.common;

import dev.shortcircuit908.punisher.common.utils.WorldVector;

import java.util.List;

/**
 * @author Caleb Milligan
 * Created on 6/29/2018.
 */
public interface IPunisherConfig {
	boolean hasChanged();
	
	String getName();
	
	String getHost();
	
	short getPort();
	
	String getUsername();
	
	String getPassword();
	
	WorldVector getJailLocation();
	
	void setJailLocation(WorldVector location);
	
	List<String> getIgnoreKickReasons();
	
	String getServer();
}
