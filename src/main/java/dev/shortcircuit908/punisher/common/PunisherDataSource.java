package dev.shortcircuit908.punisher.common;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.pool.HikariPool;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;
import java.util.Scanner;

/**
 * @author ShortCircuit908
 * Created on 1/29/2018.
 */
public class PunisherDataSource {
	private static DataSource primary_data_source;
	
	public static void tryConnect(String db_host, int db_port, String db_name, String db_username,
								  String db_password)
			throws SQLException, HikariPool.PoolInitializationException {
		String database_url = "jdbc:mysql://" + db_host + ":" + db_port + "/" + db_name;
		HikariConfig config = new HikariConfig();
		config.setPoolName("punisher");
		config.setMaximumPoolSize(8);
		config.setDataSourceClassName("com.mysql.cj.jdbc.MysqlDataSource");
		config.addDataSourceProperty("url", database_url);
		config.addDataSourceProperty("user", db_username);
		config.addDataSourceProperty("password", db_password);
		config.setConnectionTimeout(3000L);
		config.addDataSourceProperty("cachePrepStmts", true);
		config.addDataSourceProperty("prepStmtCacheSize", 250);
		config.addDataSourceProperty("prepStmtCacheSqlLimit", 2048);
		config.addDataSourceProperty("useServerPrepStmts", true);
		config.addDataSourceProperty("serverTimezone", "UTC");
		config.addDataSourceProperty("useSSL", false);
		primary_data_source = new HikariDataSource(config);
		primary_data_source.getConnection().close();
	}
	
	public static Optional<DataSource> getPrimaryDataSource() {
		return Optional.ofNullable(primary_data_source);
	}
	
	public static boolean isConnected() {
		return primary_data_source != null;
	}
	
	public static void executeScriptFromResource(String path) throws SQLException {
		if (primary_data_source == null) {
			throw new SQLException("Database not initialized");
		}
		String[] script =
				new Scanner(PunisherDataSource.class.getResourceAsStream(path)).useDelimiter("\\Z").next().split(";");
		try (Connection connection = primary_data_source.getConnection()) {
			try (Statement statement = connection.createStatement()) {
				connection.setAutoCommit(false);
				for (String line : script) {
					statement.addBatch(line);
				}
				statement.executeBatch();
				connection.commit();
			}
		}
	}
}
