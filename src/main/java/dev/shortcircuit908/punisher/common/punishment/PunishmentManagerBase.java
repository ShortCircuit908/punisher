package dev.shortcircuit908.punisher.common.punishment;

import dev.shortcircuit908.punisher.common.IPunisherPlugin;
import dev.shortcircuit908.punisher.common.PunisherDataSource;
import dev.shortcircuit908.punisher.common.cache.CacheMap;
import dev.shortcircuit908.punisher.common.cache.ExpirationPolicy;
import dev.shortcircuit908.punisher.common.cache.ExpiryType;
import dev.shortcircuit908.punisher.common.utils.StoredPlayerUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * @author Caleb Milligan
 * Created on 6/29/2018.
 */
public abstract class PunishmentManagerBase {
	protected final CacheMap<UUID, List<Punishment>> punishment_cache =
			new CacheMap<>(5, TimeUnit.SECONDS, ExpiryType.SINCE_CREATED, ExpirationPolicy.ACTIVE);
	protected final ArrayList<UUID> temporary_exemptions = new ArrayList<>();
	protected final Supplier<IPunisherPlugin> plugin_supplier;
	
	public PunishmentManagerBase(Supplier<IPunisherPlugin> plugin_supplier) {
		this.plugin_supplier = plugin_supplier;
	}
	
	public List<UUID> getTemporaryExemptions() {
		return temporary_exemptions;
	}
	
	public List<Punishment> getAllPunishments(UUID uuid) throws SQLException {
		if (punishment_cache.containsKey(uuid)) {
			return new ArrayList<>(punishment_cache.get(uuid).getValue());
		}
		Optional<DataSource> data_source = PunisherDataSource.getPrimaryDataSource();
		if (!data_source.isPresent()) {
			throw new SQLException("Database not initialized!");
		}
		List<Punishment> punishments = new LinkedList<>();
		try (Connection connection = data_source.get().getConnection()) {
			try (PreparedStatement statement = connection.prepareStatement(
					"SELECT `id`, `target_id`, `server`, `issuer_id`, `action`, `reason`, `issued`, `duration`, " +
							"`acknowledged`, `first_join_since_issue` FROM `punisher_punishments` WHERE LOWER" +
							"(`target_id`)=? AND (`server` IS NULL OR LOWER(`server`)=?) ORDER BY `issued`")) {
				statement.setString(1, uuid.toString());
				statement.setString(2, plugin_supplier.get().getPunisherConfig().getServer());
				try (ResultSet result = statement.executeQuery()) {
					while (result.next()) {
						punishments.add(new Punishment(
								result.getLong(1),
								UUID.fromString(result.getString(2)),
								result.getString(3),
								result.getString(4) == null ? null : UUID.fromString(result.getString(4)),
								Punishment.Action.valueOf(result.getString(5).toUpperCase()),
								result.getString(6),
								new Date(result.getTimestamp(7).getTime()),
								result.getString(8) == null ? null : Duration.parse(result.getString(8)),
								result.getBoolean(9),
								result.getTimestamp(10) == null ? null : new Date(result.getTimestamp(10).getTime())
						));
					}
				}
			}
		}
		punishment_cache.cache(uuid, punishments);
		return new ArrayList<>(punishments);
	}
	
	public List<Punishment> getActivePunishments(UUID uuid) throws SQLException {
		List<Punishment> punishments = getAllPunishments(uuid);
		ListIterator<Punishment> iterator = punishments.listIterator();
		Date now = new Date();
		while (iterator.hasNext()) {
			Punishment punishment = iterator.next();
			if (punishment.getDuration().isPresent()
					&& punishment.getTimeLeft(now).orElse(Duration.ZERO).getSeconds() <= 0) {
				iterator.remove();
			}
			else {
				switch (punishment.getAction()) {
					case UNMUTE:
						iterator.remove();
						while (iterator.hasPrevious()) {
							punishment = iterator.previous();
							if (punishment.getAction().equals(Punishment.Action.MUTE)
									|| punishment.getAction().equals(Punishment.Action.SOFTMUTE)) {
								iterator.remove();
							}
						}
						break;
					case THAW:
						iterator.remove();
						while (iterator.hasPrevious()) {
							punishment = iterator.previous();
							if (punishment.getAction().equals(Punishment.Action.FREEZE)) {
								iterator.remove();
							}
						}
						break;
					case UNJAIL:
						iterator.remove();
						while (iterator.hasPrevious()) {
							punishment = iterator.previous();
							if (punishment.getAction().equals(Punishment.Action.JAIL)) {
								iterator.remove();
							}
						}
						break;
					case UNBAN:
						iterator.remove();
						while (iterator.hasPrevious()) {
							punishment = iterator.previous();
							if (punishment.getAction().equals(Punishment.Action.BAN)) {
								iterator.remove();
							}
						}
						break;
					case IP_UNBAN:
						iterator.remove();
						while (iterator.hasPrevious()) {
							punishment = iterator.previous();
							if (punishment.getAction().equals(Punishment.Action.IP_BAN)) {
								iterator.remove();
							}
						}
						break;
				}
			}
		}
		return punishments;
	}
	
	public Punishment getMostRecentFreeze(UUID uuid) throws SQLException {
		return getMostRecentPunishment(uuid, Punishment.Action.FREEZE);
	}
	
	public Punishment getMostRecentSoftMute(UUID uuid) throws SQLException {
		return getMostRecentPunishment(uuid, Punishment.Action.SOFTMUTE);
	}
	
	public Punishment getMostRecentMute(UUID uuid) throws SQLException {
		return getMostRecentPunishment(uuid, Punishment.Action.MUTE);
	}
	
	public Punishment getMostRecentBan(UUID uuid) throws SQLException {
		return getMostRecentPunishment(uuid, Punishment.Action.BAN);
	}
	
	public Punishment getMostRecentJail(UUID uuid) throws SQLException {
		return getMostRecentPunishment(uuid, Punishment.Action.JAIL);
	}
	
	public Punishment getMostRecentIpBan(UUID uuid) throws SQLException {
		return getMostRecentPunishment(uuid, Punishment.Action.IP_BAN);
	}
	
	public Punishment getMostRecentPunishment(UUID uuid, Punishment.Action type) throws SQLException {
		List<Punishment> punishments = getActivePunishments(uuid);
		for (Punishment punishment : punishments) {
			if (punishment.getAction().equals(type)) {
				return punishment;
			}
		}
		return null;
	}
	
	public void acknowledgePunishment(long id) throws SQLException {
		Optional<DataSource> data_source = PunisherDataSource.getPrimaryDataSource();
		if (!data_source.isPresent()) {
			throw new SQLException("Database not initialized!");
		}
		try (Connection connection = data_source.get().getConnection()) {
			int affected;
			try (PreparedStatement statement = connection.prepareStatement(
					"UPDATE `punisher_punishments` SET `acknowledged`=1 WHERE `id`=?")) {
				statement.setLong(1, id);
				affected = statement.executeUpdate();
			}
			if (affected > 0) {
				try (PreparedStatement statement = connection.prepareStatement(
						"SELECT `target_id` FROM `punisher_punishments` WHERE `id`=?")) {
					statement.setLong(1, id);
					try (ResultSet result = statement.executeQuery()) {
						if (result.next()) {
							UUID target_id = UUID.fromString(result.getString(1));
							clearCache(target_id);
						}
					}
				}
			}
		}
	}
	
	public void updateFirstJoinSinceIssued(StoredPlayerUtils.PlayerInfo player, Instant timestamp) throws SQLException {
		Optional<DataSource> data_source = PunisherDataSource.getPrimaryDataSource();
		if (!data_source.isPresent()) {
			throw new SQLException("Database not initialized!");
		}
		if(!player.notInDatabase()){
			StoredPlayerUtils.flushAllPlayers();
		}
		try(Connection connection = data_source.get().getConnection()) {
			try (PreparedStatement statement = connection.prepareStatement(
					"UPDATE `punisher_punishments` SET `first_join_since_issue`=? WHERE LOWER(`target_id`)=? AND " +
							"`first_join_since_issue` IS NULL")) {
				statement.setDate(1, new java.sql.Date(timestamp.toEpochMilli()));
				statement.setInt(2, player.getDatabaseId().get());
				statement.execute();
			}
		}
	}
	
	public void clearCache() {
		punishment_cache.clear();
	}
	
	public void clearCache(UUID uuid) {
		punishment_cache.remove(uuid);
	}
	
	public void addPunishment(Punishment punishment) throws SQLException {
		Optional<DataSource> data_source = PunisherDataSource.getPrimaryDataSource();
		if (!data_source.isPresent()) {
			throw new SQLException("Database not initialized!");
		}
		try (Connection connection = data_source.get().getConnection()) {
			try (PreparedStatement statement = connection.prepareStatement("INSERT INTO `punisher_punishments` " +
					"(`target_id`, `server`, `issuer_id`, `action`, `reason`, `issued`, `duration`, `acknowledged`, " +
					"`first_join_since_issue`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)")) {
				statement.setString(1, punishment.getTargetId().toString().toLowerCase());
				statement.setString(2, punishment.getServer().orElse(null));
				statement.setString(3, punishment.getIssuerId().isPresent()
						? punishment.getIssuerId().get().toString().toLowerCase() : null);
				statement.setString(4, punishment.getAction().name());
				statement.setString(5, punishment.getReason().orElse(null));
				statement.setTimestamp(6, new Timestamp(punishment.getDateIssued().getTime()));
				statement.setString(7, punishment.getDuration().isPresent()
						? punishment.getDuration().get().toString() : null);
				statement.setBoolean(8, punishment.isAcknowledged());
				statement.setTimestamp(9, punishment.getFirstJoinSinceIssue().isPresent()
						? new Timestamp(punishment.getFirstJoinSinceIssue().get().getTime()) : null);
				statement.execute();
			}
		}
	}
}
