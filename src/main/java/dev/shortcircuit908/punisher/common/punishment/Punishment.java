package dev.shortcircuit908.punisher.common.punishment;

import java.time.Duration;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

/**
 * @author ShortCircuit908
 * Created on 3/19/2018.
 */
public class Punishment {
	private Long id;
	private UUID target_id;
	private String server;
	private UUID issuer_id;
	private Action action;
	private String reason;
	private Date issued;
	private Duration duration;
	private boolean acknowledged;
	private Date first_join_since_issue;
	
	private Punishment() {
	
	}
	
	public Punishment(long id, UUID target_id, String server, UUID issuer_id, Action action, String reason,
					  Date issued,
					  Duration duration, boolean acknowledged, Date first_join_since_issue) {
		this.id = id;
		this.issued = new Date();
		this.target_id = target_id;
		this.server = server;
		this.issuer_id = issuer_id;
		this.action = action;
		this.reason = reason;
		this.issued = issued;
		this.duration = duration;
		this.acknowledged = acknowledged;
		this.first_join_since_issue = first_join_since_issue;
	}
	
	public Optional<Long> getRowId() {
		return Optional.ofNullable(id);
	}
	
	public UUID getTargetId() {
		return target_id;
	}
	
	public Optional<String> getServer() {
		return Optional.ofNullable(server);
	}
	
	public Optional<UUID> getIssuerId() {
		return Optional.ofNullable(issuer_id);
	}
	
	public Action getAction() {
		return action;
	}
	
	public Optional<String> getReason() {
		return Optional.ofNullable(reason);
	}
	
	public Date getDateIssued() {
		return issued;
	}
	
	public Optional<Duration> getDuration() {
		return Optional.ofNullable(action.canExpire() ? duration : null);
	}
	
	public boolean isAcknowledged() {
		return acknowledged;
	}
	
	public Optional<Date> getFirstJoinSinceIssue() {
		return Optional.ofNullable(first_join_since_issue);
	}
	
	@Override
	public String toString() {
		return String.format(
				"Punishment[id=%1$s, target=%2$s, server=%3$s, issuer=%4$s, action=%5$s, reason=%6$s, issued=%7$s, " +
						"duration=%8$s, acknowledged=%9$s, first_join_since_issued=%10$s]",
				id,
				target_id,
				server,
				issuer_id,
				action,
				reason,
				issued,
				duration,
				acknowledged,
				first_join_since_issue
		);
	}
	
	public Optional<Duration> getTimeLeft() {
		return getTimeLeft(new Date());
	}
	
	public Optional<Duration> getTimeLeft(Date now) {
		if (duration == null) {
			return Optional.empty();
		}
		long seconds_since_issued = 0;
		if (first_join_since_issue != null) {
			seconds_since_issued = (now.getTime() - first_join_since_issue.getTime()) / 1000;
		}
		long seconds_left = duration.getSeconds() - seconds_since_issued;
		return Optional.of(seconds_left <= 0 ? Duration.ZERO : Duration.ofSeconds(seconds_left));
	}
	
	public enum Action {
		WARN("warned", false, "warn"),
		KICK("kicked", false, "kick"),
		MUTE("muted", true, "mute"),
		SOFTMUTE("softmuted", true, "softmute"),
		UNMUTE("unmuted", false, "unmute"),
		FREEZE("frozen", true, "freeze"),
		THAW("thawed", false, "thaw", "unfreeze"),
		JAIL("jailed", true, "jail"),
		UNJAIL("unjailed", false, "unjail"),
		BAN("banned", true, "ban"),
		UNBAN("unbanned", false, "unban", "pardon"),
		IP_BAN("IP banned", true, "ipban", "banip"),
		IP_UNBAN("IP unbanned", false, "ipunban", "unbanip", "pardonip", "ippardon");
		
		private final String[] aliases;
		private final String action;
		private final boolean can_expire;
		
		Action(String action, boolean can_expire, String... aliases) {
			this.action = action;
			this.can_expire = can_expire;
			this.aliases = aliases == null ? new String[0] : aliases;
		}
		
		public String getAction() {
			return action;
		}
		
		public String[] getAliases() {
			return aliases;
		}
		
		public boolean canExpire() {
			return can_expire;
		}
	}
}
