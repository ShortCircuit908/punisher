package dev.shortcircuit908.punisher.common.cache;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author ShortCircuit908
 * Created on 5/18/2017.
 */
public class CachedValue<T> {
	private final long created;
	private final AtomicLong last_accessed = new AtomicLong();
	private final AtomicBoolean expired = new AtomicBoolean(false);
	private final AtomicBoolean can_expire = new AtomicBoolean(true);
	private T value;
	
	public CachedValue(T value) {
		this.created = System.currentTimeMillis();
		setValue(value);
	}
	
	public T getValue() {
		this.last_accessed.set(System.currentTimeMillis());
		return this.value;
	}
	
	public void setValue(T value) {
		this.last_accessed.set(System.currentTimeMillis());
		this.value = value;
	}
	
	public T getValueSneakily() {
		return this.value;
	}
	
	public long getTimeCreated() {
		return this.created;
	}
	
	public long getTimeLastAccessed() {
		return this.last_accessed.get();
	}
	
	public void expire() {
		this.expired.set(true);
	}
	
	public boolean isExpired() {
		return this.expired.get();
	}
	
	public boolean canExpire() {
		return this.can_expire.get();
	}
	
	public void setCanExpire(boolean can_expire) {
		this.can_expire.set(can_expire);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof CachedValue)) {
			return false;
		}
		return Objects.equals(this.value, ((CachedValue) other).value);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		return Objects.hashCode(value);
	}
}
