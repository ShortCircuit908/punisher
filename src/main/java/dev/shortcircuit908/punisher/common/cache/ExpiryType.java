package dev.shortcircuit908.punisher.common.cache;

/**
 * @author ShortCircuit908
 * Created on 5/18/2017.
 */
public enum ExpiryType {
	SINCE_CREATED,
	SINCE_ACCESSED
}
