package dev.shortcircuit908.punisher.common.cache;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

/**
 * @author ShortCircuit908
 * Created on 5/18/2017.
 */
public class Cache<T> implements Set<CachedValue<T>> {
	private static final Timer expiry_timer = new Timer();
	
	private final HashSet<CachedValue<T>> values = new HashSet<>();
	private TimerTask expiry_check_task;
	private ExpiryType expiry_type;
	private ExpirationPolicy expiration_policy;
	private long expire_millis;
	private ExpireCallback<T> expire_callback;
	private long last_expired = System.currentTimeMillis();
	
	
	public Cache(long expire, TimeUnit unit, ExpiryType expiry_type, ExpirationPolicy expiration_policy) {
		setExpireTime(expire, unit);
		setExpiryType(expiry_type);
		setExpirationPolicy(expiration_policy);
	}
	
	public void setExpireTime(long expire, TimeUnit unit) {
		if (expire < 0) {
			throw new IllegalArgumentException("Expire time must be non-negative");
		}
		if (unit == null) {
			throw new IllegalArgumentException("Time unit may not be null");
		}
		this.expire_millis = TimeUnit.MILLISECONDS.convert(expire, unit);
		if (expiration_policy == ExpirationPolicy.ACTIVE) {
			startExpiryTask();
		}
		else {
			stopExpiryTask();
		}
	}
	
	private void stopExpiryTask() {
		if (this.expiry_check_task != null) {
			this.expiry_check_task.cancel();
		}
	}
	
	private void startExpiryTask() {
		stopExpiryTask();
		this.expiry_check_task = new TimerTask() {
			@Override
			public void run() {
				expireEntries();
			}
		};
		expiry_timer.schedule(this.expiry_check_task, 0, this.expire_millis);
	}
	
	private void lazyExpire() {
		long now = System.currentTimeMillis();
		if (last_expired + expire_millis < now) {
			last_expired = now;
			expireEntries(now, values.iterator());
		}
	}
	
	public void setExpiryType(ExpiryType expiry_type) {
		this.expiry_type = expiry_type;
	}
	
	public void setExpirationPolicy(ExpirationPolicy expiration_policy) {
		this.expiration_policy = expiration_policy;
		if (expiration_policy == ExpirationPolicy.ACTIVE) {
			startExpiryTask();
		}
		else {
			stopExpiryTask();
		}
	}
	
	public void setExpireCallback(ExpireCallback<T> expire_callback) {
		this.expire_callback = expire_callback;
	}
	
	@Override
	public int size() {
		lazyExpire();
		return values.size();
	}
	
	@Override
	public boolean isEmpty() {
		lazyExpire();
		return values.isEmpty();
	}
	
	@Override
	public boolean contains(Object o) {
		lazyExpire();
		return values.contains(0) || values.stream().anyMatch(tCachedValue -> {
			T value = tCachedValue.getValueSneakily();
			return value == o || (value != null && value.equals(o));
		});
	}
	
	public Iterator<CachedValue<T>> iterator() {
		lazyExpire();
		return values.iterator();
	}
	
	@Override
	public Object[] toArray() {
		lazyExpire();
		return values.toArray();
	}
	
	@Override
	public <E> E[] toArray(E[] a) {
		lazyExpire();
		return values.toArray(a);
	}
	
	public boolean cache(T value) {
		lazyExpire();
		for (CachedValue<T> cached : values) {
			T inner_value = cached.getValueSneakily();
			if (inner_value == value || (inner_value != null && inner_value.equals(value))) {
				cached.setValue(value);
				return true;
			}
		}
		return add(new CachedValue<>(value));
	}
	
	@Override
	public boolean add(CachedValue<T> value) {
		lazyExpire();
		return values.add(value);
	}
	
	@Override
	public boolean remove(Object o) {
		lazyExpire();
		return values.removeIf(cached_value -> {
			T value = cached_value.getValueSneakily();
			return o == cached_value || (o != null && o.equals(cached_value)) || value == o ||
					(value != null && value.equals(o));
		});
	}
	
	@Override
	public boolean containsAll(Collection<?> c) {
		lazyExpire();
		return values.stream().allMatch(cached_value -> {
			T value = cached_value.getValueSneakily();
			return c.stream()
					.allMatch((Predicate<Object>) o -> o == cached_value || (o != null && o.equals(cached_value))
							|| value == o || (value != null && value.equals(o)));
		});
	}
	
	@Override
	public boolean addAll(Collection<? extends CachedValue<T>> c) {
		lazyExpire();
		return values.addAll(c);
	}
	
	@Override
	public boolean removeAll(Collection<?> c) {
		lazyExpire();
		return values.removeIf(cached_value -> {
			T value = cached_value.getValueSneakily();
			return c.stream()
					.anyMatch((Predicate<Object>) o -> o == cached_value || (o != null && o.equals(cached_value))
							|| value == o || (value != null && value.equals(o)));
		});
	}
	
	@Override
	public boolean retainAll(Collection<?> c) {
		lazyExpire();
		return values.removeIf(cached_value -> {
			T value = cached_value.getValueSneakily();
			return c.stream()
					.noneMatch((Predicate<Object>) o -> o == cached_value || (o != null && o.equals(cached_value))
							|| value == o || (value != null && value.equals(o)));
		});
	}
	
	public void expireEntries() {
		expireEntries(System.currentTimeMillis(), this.iterator());
	}
	
	private void expireEntries(long now, Iterator<CachedValue<T>> iterator) {
		while (iterator.hasNext()) {
			CachedValue<T> value = iterator.next();
			if (!value.canExpire()) {
				continue;
			}
			switch (this.expiry_type) {
				case SINCE_CREATED:
					if (value.getTimeCreated() + this.expire_millis <= now) {
						value.expire();
					}
					break;
				case SINCE_ACCESSED:
					if (value.getTimeLastAccessed() + this.expire_millis <= now) {
						value.expire();
					}
					break;
			}
			if (value.isExpired()) {
				iterator.remove();
				if (this.expire_callback != null) {
					this.expire_callback.onValueExpired(value);
				}
			}
		}
	}
	
	@Override
	public void clear() {
		values.clear();
	}
	
	public interface ExpireCallback<T> {
		void onValueExpired(CachedValue<T> value);
	}
}
