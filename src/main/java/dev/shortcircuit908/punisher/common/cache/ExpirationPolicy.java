package dev.shortcircuit908.punisher.common.cache;

/**
 * @author Caleb Milligan
 * Created on 7/19/2018.
 */
public enum ExpirationPolicy {
	ACTIVE,
	LAZY
}
