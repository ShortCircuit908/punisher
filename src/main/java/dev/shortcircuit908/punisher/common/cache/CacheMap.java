package dev.shortcircuit908.punisher.common.cache;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

/**
 * An object to keep values in memory for a specified period of time
 *
 * @author ShortCircuit908
 * Created on 5/18/2017.
 */
public class CacheMap<K, V> implements Map<K, CachedValue<V>> {
	private static final Timer expiry_timer = new Timer();
	private final HashMap<K, CachedValue<V>> values = new HashMap<>();
	private TimerTask expiry_check_task;
	private ExpiryType expiry_type;
	private long expire_millis;
	private ExpireCallback<K, V> expire_callback;
	private ExpirationPolicy expiration_policy;
	private long last_expired = System.currentTimeMillis();
	
	public CacheMap(long expire, TimeUnit unit, ExpiryType expiry_type, ExpirationPolicy expiration_policy) {
		setExpireTime(expire, unit);
		setExpiryType(expiry_type);
		setExpirationPolicy(expiration_policy);
	}
	
	public void setExpireTime(long expire, TimeUnit unit) {
		if (expire < 0) {
			throw new IllegalArgumentException("Expire time must be non-negative");
		}
		if (unit == null) {
			throw new IllegalArgumentException("Time unit may not be null");
		}
		this.expire_millis = TimeUnit.MILLISECONDS.convert(expire, unit);
		if (expiration_policy == ExpirationPolicy.ACTIVE) {
			startExpiryTask();
		}
		else {
			stopExpiryTask();
		}
	}
	
	private void stopExpiryTask() {
		if (this.expiry_check_task != null) {
			this.expiry_check_task.cancel();
		}
	}
	
	private void startExpiryTask() {
		stopExpiryTask();
		this.expiry_check_task = new TimerTask() {
			@Override
			public void run() {
				expireEntries();
			}
		};
		expiry_timer.schedule(this.expiry_check_task, 0, this.expire_millis);
	}
	
	public void setExpirationPolicy(ExpirationPolicy expiration_policy) {
		this.expiration_policy = expiration_policy;
		if (expiration_policy == ExpirationPolicy.ACTIVE) {
			startExpiryTask();
		}
		else {
			stopExpiryTask();
		}
	}
	
	/**
	 * Sets the expiry type of this cache
	 *
	 * @param expiry_type {@link ExpiryType#SINCE_CREATED} or {@link ExpiryType#SINCE_ACCESSED}
	 * @throws IllegalArgumentException if {@code expiry_type} is null
	 * @see ExpiryType
	 */
	public void setExpiryType(ExpiryType expiry_type) {
		if (expiry_type == null) {
			throw new IllegalArgumentException("Expiry type may not be null");
		}
		this.expiry_type = expiry_type;
	}
	
	/**
	 * @param expire_callback callback to notify when values expire
	 */
	public void setExpireCallback(ExpireCallback<K, V> expire_callback) {
		this.expire_callback = expire_callback;
	}
	
	/**
	 * Associates the specified value with the specified key in this map,
	 * wrapping the value as a {@link CachedValue<V>}
	 *
	 * @param key   key key with which the specified value is to be associated
	 * @param value value value to be associated with the specified key
	 * @return the previous value associated with <tt>key</tt>, or
	 * <tt>null</tt> if there was no mapping for <tt>key</tt>.
	 * (A <tt>null</tt> return can also indicate that the map
	 * previously associated <tt>null</tt> with <tt>key</tt>,
	 * if the implementation supports <tt>null</tt> values.)
	 * @throws UnsupportedOperationException if the <tt>put</tt> operation
	 *                                       is not supported by this map
	 * @throws ClassCastException            if the class of the specified key or value
	 *                                       prevents it from being stored in this map
	 * @throws NullPointerException          if the specified key or value is null
	 *                                       and this map does not permit null keys or values
	 * @throws IllegalArgumentException      if some property of the specified key
	 *                                       or value prevents it from being stored in this map
	 * @see java.util.Map#put(Object, Object)
	 */
	public CachedValue<V> cache(K key, V value)
			throws UnsupportedOperationException, ClassCastException, NullPointerException, IllegalArgumentException {
		lazyExpire();
		if (this.values.containsKey(key)) {
			CachedValue<V> cached = this.values.get(key);
			cached.setValue(value);
			return cached;
		}
		CachedValue<V> cached = new CachedValue<>(value);
		this.values.put(key, cached);
		return cached;
	}
	
	/**
	 * Copies all of the mappings from the specified map to this map, wrapping
	 * all values as a {@link CachedValue<V>}
	 *
	 * @param m mappings to be stored in this map
	 * @throws UnsupportedOperationException if the <tt>putAll</tt> operation
	 *                                       is not supported by this map
	 * @throws ClassCastException            if the class of a key or value in the
	 *                                       specified map prevents it from being stored in this map
	 * @throws NullPointerException          if the specified map is null, or if
	 *                                       this map does not permit null keys or values, and the
	 *                                       specified map contains null keys or values
	 * @throws IllegalArgumentException      if some property of a key or value in
	 *                                       the specified map prevents it from being stored in this map
	 * @see java.util.Map#putAll(Map)
	 */
	public void cacheAll(Map<K, V> m) throws UnsupportedOperationException, ClassCastException, NullPointerException,
			IllegalArgumentException {
		lazyExpire();
		if (m == null) {
			return;
		}
		for (Map.Entry<K, V> entry : m.entrySet()) {
			cache(entry.getKey(), entry.getValue());
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		lazyExpire();
		return values.size();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEmpty() {
		lazyExpire();
		return values.isEmpty();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsKey(Object key) throws ClassCastException, NullPointerException {
		lazyExpire();
		return values.containsKey(key);
	}
	
	/**
	 * {@inheritDoc}
	 * <p>Any {@link CachedValue} encountered will itself be checked for
	 * equality before unwrapping and comparing its held value
	 */
	@Override
	public boolean containsValue(Object value) throws ClassCastException, NullPointerException {
		lazyExpire();
		return values.entrySet().stream().anyMatch(entry -> {
			CachedValue<V> inner_value = entry.getValue();
			V inner_value_value = entry.getValue().getValueSneakily();
			return inner_value == value || inner_value.equals(value) || inner_value_value == value
					|| inner_value_value != null && inner_value_value.equals(value);
		});
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public CachedValue<V> get(Object key) throws ClassCastException, NullPointerException {
		lazyExpire();
		return values.get(key);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public CachedValue<V> put(K key, CachedValue<V> value) throws UnsupportedOperationException, ClassCastException,
			NullPointerException, IllegalArgumentException {
		return values.put(key, value);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public CachedValue<V> remove(Object key) throws UnsupportedOperationException, ClassCastException,
			NullPointerException {
		lazyExpire();
		return values.remove(key);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void putAll(Map<? extends K, ? extends CachedValue<V>> m) throws UnsupportedOperationException,
			ClassCastException, NullPointerException, IllegalArgumentException {
		lazyExpire();
		values.putAll(m);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear() throws UnsupportedOperationException {
		values.clear();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<K> keySet() {
		lazyExpire();
		return values.keySet();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<CachedValue<V>> values() {
		lazyExpire();
		return values.values();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<Entry<K, CachedValue<V>>> entrySet() {
		lazyExpire();
		return values.entrySet();
	}
	
	public void expireEntries() {
		expireEntries(System.currentTimeMillis(), this.entrySet().iterator());
	}
	
	private void expireEntries(long now, Iterator<Map.Entry<K, CachedValue<V>>> iterator) {
		while (iterator.hasNext()) {
			Map.Entry<K, CachedValue<V>> entry = iterator.next();
			CachedValue<V> value = entry.getValue();
			if (!value.canExpire()) {
				continue;
			}
			switch (this.expiry_type) {
				case SINCE_CREATED:
					if (value.getTimeCreated() + this.expire_millis <= now) {
						value.expire();
					}
					break;
				case SINCE_ACCESSED:
					if (value.getTimeLastAccessed() + this.expire_millis <= now) {
						value.expire();
					}
					break;
			}
			if (value.isExpired()) {
				iterator.remove();
				if (this.expire_callback != null) {
					this.expire_callback.onValueExpired(entry.getKey(), value);
				}
			}
		}
	}
	
	private void lazyExpire() {
		long now = System.currentTimeMillis();
		if (last_expired + expire_millis < now) {
			last_expired = now;
			expireEntries(now, values.entrySet().iterator());
		}
	}
	
	public interface ExpireCallback<K, V> {
		/**
		 * @param key
		 * @param value
		 */
		void onValueExpired(K key, CachedValue<V> value);
	}
}
