package dev.shortcircuit908.punisher.common;

/**
 * @author Caleb Milligan
 * Created on 6/29/2018.
 */
public interface IPunisherPlugin {
	IPunisherConfig getPunisherConfig();
}
