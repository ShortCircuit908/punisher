package dev.shortcircuit908.punisher.sponge.commands;

import dev.shortcircuit908.punisher.sponge.Punisher;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

/**
 * @author ShortCircuit908
 * Created on 3/23/2018.
 */
public class ReloadCommand implements CommandExecutor {
	public ReloadCommand() {
		CommandSpec spec = CommandSpec.builder()
				.executor(this)
				.permission("punisher.command.reload")
				.description(Text.of("Reload the plugin"))
				.build();
		Sponge.getCommandManager().register(Punisher.getInstance(), spec, "reloadpunisher", "rlpunisher");
	}
	
	@Override
	public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
		Punisher.getInstance().reload();
		src.sendMessages(Text.of(TextColors.GOLD, "Punisher reloaded!"));
		return CommandResult.success();
	}
}
