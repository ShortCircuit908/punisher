package dev.shortcircuit908.punisher.sponge.commands;

import dev.shortcircuit908.punisher.common.utils.WorldVector;
import dev.shortcircuit908.punisher.sponge.Punisher;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Locatable;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.io.IOException;

/**
 * @author ShortCircuit908
 * Created on 3/23/2018.
 */
public class SetJailCommand implements CommandExecutor {
	public SetJailCommand() {
		CommandSpec spec = CommandSpec.builder()
				.executor(this)
				.permission("punisher.command.jail.set")
				.description(Text.of("Set the jail location"))
				.build();
		Sponge.getCommandManager().register(Punisher.getInstance(), spec, "setjail");
	}
	
	@Override
	public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
		if (!(src instanceof Locatable)) {
			throw new CommandException(Text.of("This command must be used ingame"));
		}
		Location<World> location = ((Locatable) src).getLocation();
		float pitch = 0;
		float yaw = 0;
		if (src instanceof Entity) {
			pitch = (float) ((Entity) src).getRotation().getX();
			yaw = (float) ((Entity) src).getRotation().getY();
		}
		Punisher.getInstance()
				.getPunisherConfig()
				.setJailLocation(new WorldVector(location.getExtent().getName(), location.getX(), location.getY(),
						location.getZ(), pitch, yaw));
		try {
			Punisher.getInstance().saveConfig();
		}
		catch (IOException e) {
			throw new CommandException(Text.of(e.getMessage()));
		}
		src.sendMessage(Text.of(TextColors.GOLD, "Set jail location to your position"));
		return CommandResult.success();
	}
}
