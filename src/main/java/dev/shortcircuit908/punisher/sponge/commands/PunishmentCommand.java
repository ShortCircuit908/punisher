package dev.shortcircuit908.punisher.sponge.commands;

import com.google.common.base.Joiner;
import dev.shortcircuit908.punisher.common.punishment.Punishment;
import dev.shortcircuit908.punisher.common.utils.DurationUtils;
import dev.shortcircuit908.punisher.common.utils.StoredPlayerUtils;
import dev.shortcircuit908.punisher.sponge.Punisher;
import dev.shortcircuit908.punisher.sponge.utils.UserUtils;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;

import java.sql.SQLException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @author ShortCircuit908
 * Created on 3/23/2018.
 */
public class PunishmentCommand implements CommandExecutor {
	private final Punishment.Action type;
	private final String really_dumb_thing = "@duration>] [<reason?>] [<--global";
	private final String really_dumb_thing_no_duration = "reason?>] [<--global";
	
	public PunishmentCommand(Punishment.Action type, String description, String permission) {
		this.type = type;
		CommandSpec.Builder spec = CommandSpec.builder()
				.description(Text.of(description))
				.permission(permission)
				.executor(this);
		if (type.canExpire()) {
			spec.arguments(
					GenericArguments.string(Text.of("player")),
					GenericArguments.optional(
							GenericArguments.remainingRawJoinedStrings(Text.of(really_dumb_thing))
					)
			);
		}
		else {
			spec.arguments(
					GenericArguments.string(Text.of("player")),
					GenericArguments.optional(
							GenericArguments.remainingRawJoinedStrings(Text.of(really_dumb_thing_no_duration))
					)
			);
		}
		Sponge.getCommandManager().register(Punisher.getInstance(), spec.build(), type.getAliases());
	}
	
	@Override
	public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
		String[] raw_args;
		if (args.hasAny(really_dumb_thing)) {
			raw_args = args.<String>getOne(really_dumb_thing).get().split("\\s");
		}
		else if (args.hasAny(really_dumb_thing_no_duration)) {
			raw_args = args.<String>getOne(really_dumb_thing_no_duration).get().split("\\s");
		}
		else {
			raw_args = new String[0];
		}
		List<String> args_list = new LinkedList<>(Arrays.asList(raw_args));
		String target_name = args.<String>getOne("player").get();
		StoredPlayerUtils.PlayerInfo target = null;
		try {
			List<StoredPlayerUtils.PlayerInfo> targets = StoredPlayerUtils.getExistingPlayer(target_name);
			if (targets.isEmpty()) {
				throw new CommandException(Text.of("Unknown player: " + target_name));
			}
			if (targets.size() > 1) {
				boolean exact_match = false;
				for (StoredPlayerUtils.PlayerInfo potential_target : targets) {
					if (potential_target.getUsername().orElse("{unknown}").equalsIgnoreCase(target_name)) {
						target = potential_target;
						exact_match = true;
						break;
					}
				}
				if (!exact_match) {
					List<String> names = new ArrayList<>(targets.size());
					for (StoredPlayerUtils.PlayerInfo check : targets) {
						names.add(check.getUsername().orElse("{unknown}"));
					}
					throw new CommandException(Text.of("Ambiguous player names: " + Joiner.on(", ").join(names)));
				}
			}
			else {
				target = targets.get(0);
			}
			Duration duration = null;
			if (!args_list.isEmpty()) {
				String temp_duration = args_list.get(0);
				if (temp_duration.startsWith("@")) {
					args_list.remove(0);
					try {
						duration = DurationUtils.fromString(temp_duration.substring(1));
					}
					catch (IllegalArgumentException e) {
						throw new CommandException(Text.of(e.getMessage()));
					}
				}
			}
			boolean is_global = false;
			if (!args_list.isEmpty() && args_list.get(args_list.size() - 1).equalsIgnoreCase("--global")) {
				args_list.remove(args_list.size() - 1);
				is_global = true;
			}
			// Remaining args as reason
			String reason = Joiner.on(' ').join(args_list);
			if (reason.trim().isEmpty()) {
				reason = null;
			}
			User user = UserUtils.getUser(target.getUniqueId()).orElse(null);
			Punishment punishment = new Punishment(-1, target.getUniqueId(),
					is_global ? null : Punisher.getInstance().getPunisherConfig().getServer(),
					src instanceof User ? ((User) src).getUniqueId() : null, type, reason,
					new Date(), duration, false, user != null && user.isOnline() ? new Date() : null);
			Punisher.getInstance().getPunishmentManager().addPunishment(punishment);
		}
		catch (SQLException e) {
			e.printStackTrace();
			throw new CommandException(Text.of(e.getMessage()));
		}
		return CommandResult.success();
	}
}
