package dev.shortcircuit908.punisher.sponge.commands;

import dev.shortcircuit908.punisher.common.punishment.Punishment;
import dev.shortcircuit908.punisher.sponge.Punisher;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * @author ShortCircuit908
 * Created on 3/28/2018.
 */
public class AcknowledgeCommand implements CommandExecutor {
	
	public AcknowledgeCommand() {
		CommandSpec spec = CommandSpec.builder()
				.description(Text.of("Acknowledge an action taken against you"))
				.permission("punisher.command.acknowledge")
				.arguments(
						GenericArguments.longNum(Text.of("id"))
				)
				.executor(this)
				.build();
		Sponge.getCommandManager().register(Punisher.getInstance(), spec, "acknowledge");
	}
	
	@Override
	public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
		if (!(src instanceof User)) {
			throw new CommandException(Text.of("This command is player-only"));
		}
		long id = args.<Long>getOne("id").orElse(-1L);
		try {
			List<Punishment> punishments =
					Punisher.getInstance().getPunishmentManager().getAllPunishments(((User) src).getUniqueId());
			Optional<Long> row_id;
			for (Punishment punishment : punishments) {
				row_id = punishment.getRowId();
				if (row_id.isPresent() && row_id.get() == id) {
					if (punishment.isAcknowledged()) {
						src.sendMessage(Text.of(TextColors.GOLD, "You have already acknowledged this action"));
					}
					else {
						Punisher.getInstance().getPunishmentManager().acknowledgePunishment(id);
						src.sendMessage(Text.of(TextColors.GOLD, "Action acknowledged"));
					}
					return CommandResult.success();
				}
			}
		}
		catch (SQLException e) {
			throw new CommandException(Text.of("An error has occurred"), e);
		}
		src.sendMessage(Text.of(TextColors.GOLD, "No action has been found with ID ", TextColors.RED, id));
		return CommandResult.success();
	}
}
