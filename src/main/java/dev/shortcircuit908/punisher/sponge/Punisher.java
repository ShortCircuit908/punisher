package dev.shortcircuit908.punisher.sponge;

import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import dev.shortcircuit908.punisher.common.IPunisherConfig;
import dev.shortcircuit908.punisher.common.IPunisherPlugin;
import dev.shortcircuit908.punisher.common.PunisherDataSource;
import dev.shortcircuit908.punisher.common.punishment.Punishment;
import dev.shortcircuit908.punisher.common.utils.WorldVector;
import dev.shortcircuit908.punisher.sponge.commands.AcknowledgeCommand;
import dev.shortcircuit908.punisher.sponge.commands.PunishmentCommand;
import dev.shortcircuit908.punisher.sponge.commands.ReloadCommand;
import dev.shortcircuit908.punisher.sponge.commands.SetJailCommand;
import dev.shortcircuit908.punisher.sponge.listeners.PlayerListener;
import dev.shortcircuit908.punisher.sponge.listeners.PunishmentListener;
import dev.shortcircuit908.punisher.sponge.punishment.PunishmentManagerSponge;
import dev.shortcircuit908.punisher.sponge.utils.LocationSerializer;
import dev.shortcircuit908.punisher.sponge.utils.WorldSerializer;
import com.zaxxer.hikari.pool.HikariPool;
import ninja.leaping.configurate.ConfigurationOptions;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializerCollection;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.config.ConfigDir;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartingServerEvent;
import org.spongepowered.api.event.game.state.GameStoppingServerEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.io.IOException;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.List;

/**
 * @author ShortCircuit908
 * Created on 3/23/2018.
 */
@Plugin(
		id = Punisher.PLUGIN_IDENTIFIER,
		name = "Punisher",
		version = "1.0-SNAPSHOT",
		description = "Powerful punishment plugin",
		authors = {"ShortCircuit908"}
)
public class Punisher implements IPunisherPlugin {
	public static final String PLUGIN_IDENTIFIER = "punisher";
	private static Punisher instance;
	
	private final PunishmentManagerSponge punishment_manager = new PunishmentManagerSponge();
	
	@Inject
	private Logger logger;
	
	@Inject
	@ConfigDir(sharedRoot = false)
	private Path config_path;
	
	@Inject
	@DefaultConfig(sharedRoot = false)
	private ConfigurationLoader<CommentedConfigurationNode> config_loader;
	private ConfigurationOptions options;
	
	private CommentedConfigurationNode config_node;
	
	public Punisher() {
		instance = this;
	}
	
	public static Punisher getInstance() {
		return instance;
	}
	
	@Listener
	public void onInitialize(final GameInitializationEvent event) {
		TypeToken<Location<World>> location_type = new TypeToken<Location<World>>() {
		};
		TypeSerializerCollection serializers = config_loader.getDefaultOptions().getSerializers();
		serializers.registerType(TypeToken.of(World.class), new WorldSerializer());
		serializers.registerType(location_type, new LocationSerializer());
		config_loader.getDefaultOptions().setSerializers(serializers);
	}
	
	@Listener
	public void onServerStarting(final GameStartingServerEvent event) {
		Sponge.getEventManager().registerListeners(this, new PlayerListener());
		Sponge.getEventManager().registerListeners(this, new PunishmentListener());
		new PunishmentCommand(Punishment.Action.BAN, "Ban a user", "punisher.command.ban");
		new PunishmentCommand(Punishment.Action.UNBAN, "Unban a user", "punisher.command.ban.unban");
		new PunishmentCommand(Punishment.Action.IP_BAN, "Ban a user's last known IP address", "punisher.command" +
				".ipban");
		new PunishmentCommand(Punishment.Action.IP_UNBAN, "Unban a user's last known IP address",
				"punisher.command.ipban.unban");
		new PunishmentCommand(Punishment.Action.MUTE, "Mute a user", "punisher.command.mute");
		new PunishmentCommand(Punishment.Action.SOFTMUTE, "Softmute a user", "punisher.command.softmute");
		new PunishmentCommand(Punishment.Action.UNMUTE, "Unmute a user", "punisher.command.mute.unmute");
		new PunishmentCommand(Punishment.Action.FREEZE, "Freeze a user in place", "punisher.command.freeze");
		new PunishmentCommand(Punishment.Action.THAW, "Un-freeze a user", "punisher.command.freeze.unfreeze");
		new PunishmentCommand(Punishment.Action.WARN, "Give a warning to a user", "punisher.command.warn");
		new PunishmentCommand(Punishment.Action.KICK, "Kick a user from the server", "punisher.command.kick");
		new PunishmentCommand(Punishment.Action.JAIL, "Send a user to jail", "punisher.command.jail");
		new PunishmentCommand(Punishment.Action.UNJAIL, "Releasea user from jail", "punisher.command.jail.unjail");
		new ReloadCommand();
		new SetJailCommand();
		new AcknowledgeCommand();
		reload();
	}
	
	@Listener
	public void onServerStopping(final GameStoppingServerEvent event) {
	
	}
	
	public void reload() {
		try {
			loadConfig();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		PunisherConfig config = getPunisherConfig();
		try {
			PunisherDataSource.tryConnect(config.getHost(), config.getPort(), config.getName(), config.getUsername(),
					config.getPassword());
			PunisherDataSource.executeScriptFromResource("/schema_blank.sql");
		}
		catch (HikariPool.PoolInitializationException e) {
			getLogger().error(e.getMessage());
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		punishment_manager.clearCache();
	}
	
	public PunishmentManagerSponge getPunishmentManager() {
		return punishment_manager;
	}
	
	public Logger getLogger() {
		return logger;
	}
	
	public void loadConfig() throws IOException {
		config_node = config_loader.load();
		PunisherConfig config = getPunisherConfig();
		config.getServer();
		config.getIgnoreKickReasons();
		config.getHost();
		config.getPort();
		config.getName();
		config.getUsername();
		config.getPassword();
		if (config.hasChanged()) {
			saveConfig();
		}
		logger.info("Configuration loaded");
	}
	
	public void saveConfig() throws IOException {
		config_loader.save(config_node);
		logger.info("Configuration saved");
	}
	
	public PunisherConfig getPunisherConfig() {
		return new PunisherConfig(config_node);
	}
	
	public class PunisherConfig implements IPunisherConfig {
		private final CommentedConfigurationNode root_node;
		private boolean has_changed = false;
		
		private PunisherConfig(CommentedConfigurationNode root_node) {
			this.root_node = root_node;
		}
		
		@Override
		public boolean hasChanged() {
			return has_changed;
		}
		
		@Override
		public String getServer() {
			CommentedConfigurationNode node = root_node.getNode("server-name");
			if (node.isVirtual()) {
				has_changed = true;
				node.setValue("My Minecraft Server");
			}
			return node.getString("My Minecraft Server");
		}
		
		@Override
		public List<String> getIgnoreKickReasons() {
			CommentedConfigurationNode node = root_node.getNode("ignore-kick-reasons");
			List<String> default_value = Lists.newArrayList("server is .*", ".*\\b(afk|idle|idling)\\b.*");
			if (node.isVirtual()) {
				has_changed = true;
				TypeToken<List<String>> type = new TypeToken<List<String>>() {
				};
				try {
					node.setValue(type, default_value);
				}
				catch (ObjectMappingException e) {
					// Do nothing
				}
			}
			try {
				return node.getList(TypeToken.of(String.class), default_value);
			}
			catch (ObjectMappingException e) {
				e.printStackTrace();
			}
			return default_value;
		}
		
		@Override
		public WorldVector getJailLocation() {
			CommentedConfigurationNode node = root_node.getNode("jail");
			TypeToken<WorldVector> type = new TypeToken<WorldVector>() {
			};
			try {
				return node.getValue(type);
			}
			catch (ObjectMappingException e) {
				e.printStackTrace();
			}
			return null;
		}
		
		@Override
		public void setJailLocation(WorldVector location) {
			CommentedConfigurationNode node = root_node.getNode("jail");
			TypeToken<WorldVector> type = new TypeToken<WorldVector>() {
			};
			try {
				node.setValue(type, location);
				has_changed = true;
			}
			catch (ObjectMappingException e) {
				e.printStackTrace();
			}
		}
		
		@Override
		public String getName() {
			CommentedConfigurationNode node = root_node.getNode("db", "name");
			if (node.isVirtual()) {
				has_changed = true;
				node.setValue("");
			}
			return node.getString("");
		}
		
		@Override
		public String getHost() {
			CommentedConfigurationNode node = root_node.getNode("db", "host");
			if (node.isVirtual()) {
				has_changed = true;
				node.setValue("localhost");
			}
			return node.getString("localhost");
		}
		
		@Override
		public short getPort() {
			CommentedConfigurationNode node = root_node.getNode("db", "port");
			if (node.isVirtual()) {
				has_changed = true;
				node.setValue(3306);
			}
			return (short) node.getInt(3306);
		}
		
		@Override
		public String getUsername() {
			CommentedConfigurationNode node = root_node.getNode("db", "username");
			if (node.isVirtual()) {
				has_changed = true;
				node.setValue("root");
			}
			return node.getString("root");
		}
		
		@Override
		public String getPassword() {
			CommentedConfigurationNode node = root_node.getNode("db", "password");
			if (node.isVirtual()) {
				has_changed = true;
				node.setValue("");
			}
			return node.getString("");
		}
	}
}
