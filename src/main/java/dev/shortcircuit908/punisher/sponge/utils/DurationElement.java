package dev.shortcircuit908.punisher.sponge.utils;

import dev.shortcircuit908.punisher.common.utils.DurationUtils;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.ArgumentParseException;
import org.spongepowered.api.command.args.CommandArgs;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.text.Text;
import org.spongepowered.plugin.meta.util.NonnullByDefault;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ShortCircuit908
 * Created on 3/23/2018.
 */
public class DurationElement extends CommandElement {
	
	public DurationElement(Text key) {
		super(key);
	}
	
	@Nullable
	@Override
	protected Object parseValue(CommandSource source, CommandArgs args) throws ArgumentParseException {
		String s = args.next();
		if (!s.startsWith("@")) {
			return null;
		}
		s = s.substring(1);
		try {
			return DurationUtils.fromString(s);
		}
		catch (IllegalArgumentException e) {
			throw args.createError(Text.of("Invalid duration!"));
		}
	}
	
	@NonnullByDefault
	@Override
	public List<String> complete(CommandSource src, CommandArgs args, CommandContext context) {
		return new ArrayList<>(0);
	}
}