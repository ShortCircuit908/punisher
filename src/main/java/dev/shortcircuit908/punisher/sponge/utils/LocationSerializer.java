package dev.shortcircuit908.punisher.sponge.utils;

import com.google.common.reflect.TypeToken;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

/**
 * @author ShortCircuit908
 * Created on 1/16/2018.
 */
public class LocationSerializer implements TypeSerializer<Location<World>> {
	@Override
	public Location<World> deserialize(TypeToken<?> type, ConfigurationNode value) {
		ConfigurationNode node_world = value.getNode("world");
		ConfigurationNode node_x = value.getNode("x");
		ConfigurationNode node_y = value.getNode("y");
		ConfigurationNode node_z = value.getNode("z");
		if (node_world.isVirtual() || node_x.isVirtual() || node_y.isVirtual() || node_z.isVirtual()) {
			return null;
		}
		World world = new WorldSerializer().deserialize(TypeToken.of(World.class), node_world);
		if (world == null) {
			return null;
		}
		return new Location<>(world, node_x.getDouble(), node_y.getDouble(), node_z.getDouble());
	}
	
	@Override
	public void serialize(TypeToken<?> type, Location<World> obj, ConfigurationNode value) {
		if (obj == null) {
			value.setValue(null);
		}
		else {
			ConfigurationNode node_world = value.getNode("world");
			ConfigurationNode node_x = value.getNode("x");
			ConfigurationNode node_y = value.getNode("y");
			ConfigurationNode node_z = value.getNode("z");
			new WorldSerializer().serialize(TypeToken.of(World.class), obj.getExtent(), node_world);
			node_x.setValue(obj.getX());
			node_y.setValue(obj.getY());
			node_z.setValue(obj.getZ());
		}
	}
}
