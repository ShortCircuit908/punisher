package dev.shortcircuit908.punisher.sponge.utils;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.profile.GameProfile;
import org.spongepowered.api.service.ProvisioningException;
import org.spongepowered.api.service.user.UserStorageService;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * @author ShortCircuit908
 * Created on 1/8/2018.
 */
public class UserUtils {
	private static UserStorageService user_storage_service;
	
	public static Optional<User> getUser(UUID uuid) {
		if (uuid == null) {
			return Optional.empty();
		}
		for (Player player : Sponge.getServer().getOnlinePlayers()) {
			if (player.getUniqueId().equals(uuid)) {
				return Optional.of(player);
			}
		}
		ensureUserStorageService();
		return user_storage_service.get(uuid);
	}
	
	public static Optional<User> getUser(GameProfile profile) {
		if (profile == null) {
			return Optional.empty();
		}
		for (Player player : Sponge.getServer().getOnlinePlayers()) {
			if (player.getUniqueId().equals(profile.getUniqueId()) ||
					player.getName().equalsIgnoreCase(profile.getName().orElse(null))) {
				return Optional.of(player);
			}
		}
		ensureUserStorageService();
		return user_storage_service.get(profile);
	}
	
	public static Optional<User> getUser(String last_known_name) {
		if (last_known_name == null) {
			return Optional.empty();
		}
		for (Player player : Sponge.getServer().getOnlinePlayers()) {
			if (player.getName().equalsIgnoreCase(last_known_name)) {
				return Optional.of(player);
			}
		}
		ensureUserStorageService();
		return user_storage_service.get(last_known_name);
	}
	
	public static Optional<User> getFirstMatchingUser(Pattern pattern) {
		if (pattern == null) {
			return Optional.empty();
		}
		for (Player player : Sponge.getServer().getOnlinePlayers()) {
			if (pattern.matcher(player.getName()).matches()) {
				return Optional.of(player);
			}
		}
		ensureUserStorageService();
		Collection<GameProfile> profiles = user_storage_service.getAll();
		for (GameProfile profile : profiles) {
			Optional<String> name_opt = profile.getName();
			if (name_opt.isPresent()) {
				if (pattern.matcher(name_opt.get()).matches()) {
					return getUser(profile);
				}
			}
		}
		return Optional.empty();
	}
	
	public static List<User> getMatchingUsers(Pattern pattern) {
		if (pattern == null) {
			return new ArrayList<>(0);
		}
		ensureUserStorageService();
		Collection<GameProfile> profiles = user_storage_service.getAll();
		ArrayList<User> matching = new ArrayList<>(profiles.size());
		for (GameProfile profile : profiles) {
			Optional<String> name_opt = profile.getName();
			if (name_opt.isPresent()) {
				if (pattern.matcher(name_opt.get()).matches()) {
					getUser(profile).ifPresent(matching::add);
				}
			}
		}
		matching.trimToSize();
		return matching;
	}
	
	public static Optional<User> getFirstMatchingUser(String part) {
		if (part == null) {
			return Optional.empty();
		}
		for (Player player : Sponge.getServer().getOnlinePlayers()) {
			if (player.getName().toLowerCase().startsWith(part.toLowerCase())) {
				return Optional.of(player);
			}
		}
		ensureUserStorageService();
		Collection<GameProfile> profiles = user_storage_service.getAll();
		for (GameProfile profile : profiles) {
			Optional<String> name_opt = profile.getName();
			if (name_opt.isPresent()) {
				if (name_opt.get().toLowerCase().startsWith(part.toLowerCase())) {
					return getUser(profile);
				}
			}
		}
		return Optional.empty();
	}
	
	public static List<User> getMatchingUsers(@Nonnull String part) {
		ensureUserStorageService();
		Collection<GameProfile> profiles = user_storage_service.getAll();
		ArrayList<User> matching = new ArrayList<>(profiles.size());
		for (GameProfile profile : profiles) {
			Optional<String> name_opt = profile.getName();
			if (name_opt.isPresent()) {
				if (name_opt.get().toLowerCase().startsWith(part.toLowerCase())) {
					getUser(profile).ifPresent(matching::add);
				}
			}
		}
		matching.trimToSize();
		return matching;
	}
	
	private static void ensureUserStorageService() {
		if (user_storage_service == null) {
			Optional<UserStorageService> user_storage_service_opt =
					Sponge.getServiceManager().provide(UserStorageService.class);
			user_storage_service =
					user_storage_service_opt.orElseThrow(() -> new ProvisioningException(UserStorageService.class));
		}
	}
}
