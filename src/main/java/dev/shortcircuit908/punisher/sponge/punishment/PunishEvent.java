package dev.shortcircuit908.punisher.sponge.punishment;

import dev.shortcircuit908.punisher.common.punishment.Punishment;
import dev.shortcircuit908.punisher.sponge.utils.UserUtils;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.event.Cancellable;
import org.spongepowered.api.event.Event;
import org.spongepowered.api.event.cause.Cause;

import java.time.Duration;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

/**
 * @author ShortCircuit908
 * Created on 3/23/2018.
 */
public class PunishEvent implements Event, Cancellable {
	private final Punishment punishment;
	private final Cause cause;
	private boolean cancelled = false;
	private boolean persistent = true;
	
	public PunishEvent(Punishment punishment, Cause cause) {
		this.punishment = punishment;
		this.cause = cause;
	}
	
	public Punishment.Action getAction() {
		return punishment.getAction();
	}
	
	public Optional<String> getReason() {
		return punishment.getReason();
	}
	
	public User getTarget() {
		return UserUtils.getUser(punishment.getTargetId()).get();
	}
	
	public Optional<User> getIssuer() {
		Optional<UUID> uuid_opt = punishment.getIssuerId();
		return uuid_opt.map(uuid -> UserUtils.getUser(uuid).get());
	}
	
	public Optional<Duration> getDuration() {
		return punishment.getDuration();
	}
	
	public Date getDateIssued() {
		return punishment.getDateIssued();
	}
	
	public Optional<String> getServer() {
		return punishment.getServer();
	}
	
	public boolean isIssuedByConsole() {
		return !punishment.getIssuerId().isPresent();
	}
	
	public Punishment getPunishment() {
		return punishment;
	}
	
	public boolean isPersistent() {
		return persistent;
	}
	
	public void setPersistent(boolean persist) {
		this.persistent = persist;
	}
	
	@Override
	public Cause getCause() {
		return cause;
	}
	
	@Override
	public boolean isCancelled() {
		return cancelled;
	}
	
	@Override
	public void setCancelled(boolean cancel) {
		this.cancelled = cancel;
	}
}
