package dev.shortcircuit908.punisher.sponge.punishment;

import com.flowpowered.math.vector.Vector3d;
import dev.shortcircuit908.punisher.common.cache.CacheMap;
import dev.shortcircuit908.punisher.common.cache.ExpirationPolicy;
import dev.shortcircuit908.punisher.common.cache.ExpiryType;
import dev.shortcircuit908.punisher.common.punishment.Punishment;
import dev.shortcircuit908.punisher.common.punishment.PunishmentManagerBase;
import dev.shortcircuit908.punisher.common.utils.DurationUtils;
import dev.shortcircuit908.punisher.common.utils.StoredPlayerUtils;
import dev.shortcircuit908.punisher.common.utils.Tuple;
import dev.shortcircuit908.punisher.common.utils.WorldVector;
import dev.shortcircuit908.punisher.sponge.Punisher;
import dev.shortcircuit908.punisher.sponge.utils.DumbCompatibilityStuff;
import dev.shortcircuit908.punisher.sponge.utils.UserUtils;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author ShortCircuit908
 * Created on 3/19/2018.
 */
public class PunishmentManagerSponge extends PunishmentManagerBase {
	private final CacheMap<UUID, List<Punishment>> punishment_cache =
			new CacheMap<>(5, TimeUnit.MINUTES, ExpiryType.SINCE_CREATED, ExpirationPolicy.ACTIVE);
	
	public PunishmentManagerSponge() {
		super(Punisher::getInstance);
	}
	
	public void addPunishment(Punishment punishment) throws SQLException {
		PunishEvent event = new PunishEvent(punishment, DumbCompatibilityStuff.source(Punisher.getInstance()));
		Sponge.getEventManager().post(event);
		if (event.isCancelled()) {
			return;
		}
		if (event.isPersistent()) {
			super.addPunishment(punishment);
			Punisher.getInstance().getLogger().info("Logged punishment: " + punishment);
		}
		else {
			Punisher.getInstance().getLogger().info("Handled non-persistent punishment: " + punishment);
			Punisher.getInstance().getLogger().info("The punishment will not be logged and will not reflect in-game");
		}
		punishment_cache.remove(punishment.getTargetId());
		Optional<User> offline_target = UserUtils.getUser(punishment.getTargetId());
		Optional<Player> target = offline_target.flatMap(User::getPlayer);
		Tuple<Text> messages = generatePunishmentMessage(punishment);
		Text self_reason = messages.get(0);
		Text other_reason = messages.get(1);
		if (target.isPresent()) {
			switch (punishment.getAction()) {
				case SOFTMUTE:
					break;
				case BAN:
				case IP_BAN:
				case KICK:
					target.get().kick(self_reason);
					break;
				case JAIL:
					applyJail(target.get());
					target.get().sendMessage(self_reason);
					break;
				case UNJAIL:
					target.get()
							.setLocation(Sponge.getServer()
									.getWorld(Sponge.getServer().getDefaultWorldName())
									.get()
									.getSpawnLocation());
				default:
					target.get().sendMessage(self_reason);
					break;
			}
		}
		for (Player player : Sponge.getServer().getOnlinePlayers()) {
			if (player.hasPermission("punisher.receive_notifications")) {
				player.sendMessage(other_reason);
			}
		}
	}
	
	public void applyJail(Player player) {
		WorldVector location = Punisher.getInstance().getPunisherConfig().getJailLocation();
		if (location != null) {
			temporary_exemptions.add(player.getUniqueId());
			player.setLocation(new Location<>(Sponge.getServer().getWorld(location.world).get(), location.x,
					location.y,
					location.z));
			player.setRotation(new Vector3d(location.pitch, location.yaw, 0.0));
			temporary_exemptions.remove(player.getUniqueId());
		}
	}
	
	public Tuple<Text> generatePunishmentMessage(Punishment punishment) {
		Optional<UUID> issuer_id = punishment.getIssuerId();
		String issuer_name = Sponge.getServer().getConsole().getName();
		if (issuer_id.isPresent()) {
			try {
				Optional<StoredPlayerUtils.PlayerInfo> info = StoredPlayerUtils.getExistingPlayer(issuer_id.get());
				if (info.isPresent()) {
					issuer_name = info.get().getUsername().orElse("{unknown}");
				}
				else {
					throw new SQLException();
				}
			}
			catch (SQLException e) {
				Optional<User> issuer = UserUtils.getUser(issuer_id.get());
				if (issuer.isPresent()) {
					issuer_name = issuer.get().getName();
				}
			}
		}
		String target_name = "{unknown}";
		try {
			Optional<StoredPlayerUtils.PlayerInfo> info = StoredPlayerUtils.getExistingPlayer(punishment.getTargetId());
			if (info.isPresent()) {
				target_name = info.get().getUsername().orElse("{unknown}");
			}
			else {
				throw new SQLException();
			}
		}
		catch (SQLException e) {
			Optional<User> target = UserUtils.getUser(punishment.getTargetId());
			if (target.isPresent()) {
				target_name = target.get().getName();
			}
		}
		Text.Builder reason_builder = Text.builder().color(TextColors.GOLD)
				.append(Text.of(TextColors.RED, punishment.getAction().getAction()))
				.append(Text.of(" by "))
				.append(Text.of(TextColors.RED, issuer_name));
		if (punishment.getAction().canExpire() && punishment.getTimeLeft().isPresent()) {
			reason_builder.append(Text.of(" for "))
					.append(Text.of(TextColors.RED, DurationUtils.toString(punishment.getTimeLeft().get())));
		}
		if (punishment.getReason().isPresent()) {
			reason_builder.append(Text.of(":\n"))
					.append(Text.of(TextColors.RED, punishment.getReason().get()));
		}
		Text built = reason_builder.build();
		Text self_reason = Text.of(TextColors.GOLD, "You have been ", built);
		Text other_reason = Text.of(TextColors.RED, target_name, TextColors.GOLD, " has been ", built);
		return new Tuple<>(self_reason, other_reason, built);
	}
}
