package dev.shortcircuit908.punisher.sponge.listeners;

import dev.shortcircuit908.punisher.common.punishment.Punishment;
import dev.shortcircuit908.punisher.sponge.Punisher;
import dev.shortcircuit908.punisher.sponge.punishment.PunishEvent;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.List;
import java.util.Optional;

/**
 * @author ShortCircuit908
 * Created on 3/23/2018.
 */
public class PunishmentListener {
	@Listener
	public void cancelNoJail(final PunishEvent event) {
		if (event.getAction().equals(Punishment.Action.JAIL) &&
				Punisher.getInstance().getPunisherConfig().getJailLocation() == null) {
			if (event.isIssuedByConsole()) {
				Sponge.getServer()
						.getConsole()
						.sendMessage(Text.of(TextColors.RED, "The jail location has not been set"));
			}
			else {
				Optional<Player> issuer = event.getIssuer().get().getPlayer();
				issuer.ifPresent(player -> player.sendMessage(
						Text.of(TextColors.RED, "The jail location has not been set. Set it with ",
								TextColors.GOLD, "/setjail")));
			}
			event.setCancelled(true);
		}
	}
	
	@Listener
	public void cancelShutdownKick(final PunishEvent event) {
		if (event.getAction().equals(Punishment.Action.KICK) && event.getReason().isPresent()) {
			List<String> matchers = Punisher.getInstance().getPunisherConfig().getIgnoreKickReasons();
			for (String matcher : matchers) {
				if (!matcher.startsWith("^")) {
					matcher = "^" + matcher;
				}
				if (!matcher.endsWith("$")) {
					matcher = matcher + "$";
				}
				if (event.getReason().get().toLowerCase().matches(matcher.toLowerCase())) {
					event.setPersistent(false);
					Punisher.getInstance().getLogger().info("Setting suspected auto-kick to non-persistent");
					return;
				}
			}
		}
	}
	
	@Listener
	public void ensureOnline(final PunishEvent event) {
		if (event.getAction().equals(Punishment.Action.KICK) && !event.getTarget().isOnline()) {
			event.setCancelled(true);
			Optional<User> punisher = event.getIssuer();
			Text text = Text.of(TextColors.RED, "Cannot kick ", TextColors.GOLD, event.getTarget().getName(),
					TextColors.RED, ": Player is not online");
			if (punisher.isPresent()) {
				if (punisher.get().isOnline()) {
					punisher.get().getPlayer().get().sendMessage(text);
				}
			}
			else {
				Sponge.getServer().getConsole().sendMessage(text);
			}
		}
	}
}
