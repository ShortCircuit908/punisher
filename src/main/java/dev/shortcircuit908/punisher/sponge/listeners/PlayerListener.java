package dev.shortcircuit908.punisher.sponge.listeners;

import com.flowpowered.math.vector.Vector3d;
import dev.shortcircuit908.punisher.common.PunisherDataSource;
import dev.shortcircuit908.punisher.common.punishment.Punishment;
import dev.shortcircuit908.punisher.common.utils.WorldVector;
import dev.shortcircuit908.punisher.sponge.Punisher;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandMapping;
import org.spongepowered.api.entity.Transform;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.action.InteractEvent;
import org.spongepowered.api.event.command.SendCommandEvent;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.event.item.inventory.InteractInventoryEvent;
import org.spongepowered.api.event.message.MessageChannelEvent;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.event.server.ClientPingServerEvent;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.channel.type.FixedMessageChannel;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author ShortCircuit908
 * Created on 3/23/2018.
 */
public class PlayerListener {
	@Listener(order = Order.PRE, beforeModifications = true)
	public void updatePlayerAddressBook(final ClientConnectionEvent.Login event) {
		Optional<DataSource> data_source = PunisherDataSource.getPrimaryDataSource();
		if (data_source.isPresent()) {
			try (Connection connection = data_source.get().getConnection()) {
				try (PreparedStatement statement = connection.prepareStatement(
						"REPLACE INTO `punisher_ip_address_book` (`player_id`, `address`) VALUES (?, ?)")) {
					statement.setString(1, event.getProfile().getUniqueId().toString().toLowerCase());
					statement.setString(2,
							event.getConnection().getAddress().getAddress().getHostAddress().toLowerCase());
					statement.execute();
				}
				if (event.getProfile().getName().isPresent()) {
					updatePlayerInfo(event.getProfile().getUniqueId(), event.getProfile().getName().get());
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Listener(beforeModifications = true)
	public void playerLeave(final ClientConnectionEvent.Disconnect event) {
		try {
			updatePlayerInfo(event.getTargetEntity().getUniqueId(), event.getTargetEntity().getName());
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void updatePlayerInfo(UUID uuid, String username) throws SQLException {
		Optional<DataSource> data_source = PunisherDataSource.getPrimaryDataSource();
		if (data_source.isPresent()) {
			try (Connection connection = data_source.get().getConnection()) {
				updatePlayerInfo(uuid, username, connection);
			}
		}
	}
	
	private void updatePlayerInfo(UUID uuid, String username, Connection connection) throws SQLException {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		try (PreparedStatement statement = connection.prepareStatement(
				"REPLACE INTO `punisher_uuid_map` (`id`, `username`, `last_online`) VALUES (?, ?, ?)")) {
			statement.setString(1, uuid.toString().toLowerCase());
			statement.setString(2, username);
			statement.setTimestamp(3, timestamp);
			statement.execute();
		}
		try (PreparedStatement statement = connection.prepareStatement(
				"UPDATE `punisher_punishments` SET `first_join_since_issue`=? WHERE LOWER(`target_id`)=? AND " +
						"`first_join_since_issue` IS NULL")) {
			statement.setTimestamp(1, timestamp);
			statement.setString(2, uuid.toString().toLowerCase());
			statement.execute();
		}
	}
	
	@Listener(order = Order.LAST)
	public void punishIpBan(final ClientConnectionEvent.Login event) {
		if (PunisherDataSource.isConnected()) {
			try {
				List<UUID> matching_ids = Punisher.getInstance()
						.getPunishmentManager()
						.getPlayerIdFromInetAddress(event.getConnection().getAddress().getAddress());
				for (UUID uuid : matching_ids) {
					Punishment ban = Punisher.getInstance().getPunishmentManager().getMostRecentIpBan(uuid);
					if (ban != null) {
						event.setCancelled(true);
						event.setMessage(
								Punisher.getInstance().getPunishmentManager().generatePunishmentMessage(ban).get(0));
						return;
					}
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Listener(order = Order.LAST)
	public void showAcknowledgements(final ClientConnectionEvent.Join event) {
		final Player player = event.getTargetEntity();
		Sponge.getScheduler().createTaskBuilder()
				.async()
				.delay(10, TimeUnit.SECONDS)
				.execute(new Runnable() {
					@Override
					public void run() {
						if (PunisherDataSource.isConnected() && player.isOnline()) {
							try {
								List<Punishment> punishments = Punisher.getInstance()
										.getPunishmentManager()
										.getAllPunishments(player.getUniqueId());
								Optional<Long> row_id;
								for (Punishment punishment : punishments) {
									if (!punishment.isAcknowledged() && (row_id = punishment.getRowId()).isPresent()) {
										player.sendMessage(Text.of(TextColors.GOLD, "You were recently ",
												Punisher.getInstance()
														.getPunishmentManager()
														.generatePunishmentMessage(punishment)
														.get(2),
												TextColors.GOLD, ". You must acknowledge this action by typing ",
												TextColors.RED,
												"/acknowledge ", row_id.get()));
									}
								}
							}
							catch (SQLException e) {
								e.printStackTrace();
							}
						}
					}
				})
				.submit(Punisher.getInstance());
	}
	
	@Listener(order = Order.LAST)
	public void punishBan(final ClientConnectionEvent.Login event) {
		if (PunisherDataSource.isConnected()) {
			try {
				Punishment ban = Punisher.getInstance()
						.getPunishmentManager()
						.getMostRecentBan(event.getProfile().getUniqueId());
				if (ban != null) {
					event.setCancelled(true);
					event.setMessage(
							Punisher.getInstance().getPunishmentManager().generatePunishmentMessage(ban).get(0));
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Listener(order = Order.LAST)
	public void punishBan(final ClientPingServerEvent event) {
		if (PunisherDataSource.isConnected()) {
			try {
				List<UUID> matching_ids = Punisher.getInstance()
						.getPunishmentManager()
						.getPlayerIdFromInetAddress(event.getClient().getAddress().getAddress());
				for (UUID uuid : matching_ids) {
					Punishment ban = Punisher.getInstance().getPunishmentManager().getMostRecentBan(uuid);
					if (ban == null) {
						ban = Punisher.getInstance().getPunishmentManager().getMostRecentIpBan(uuid);
					}
					if (ban != null) {
						event.getResponse()
								.setDescription(Punisher.getInstance()
										.getPunishmentManager()
										.generatePunishmentMessage(ban)
										.get(0));
						return;
					}
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Listener(order = Order.LAST)
	public void punishMute(final MessageChannelEvent.Chat event, @Root Player player) {
		if (PunisherDataSource.isConnected()) {
			try {
				Punishment punishment =
						Punisher.getInstance().getPunishmentManager().getMostRecentMute(player.getUniqueId());
				if (punishment != null) {
					event.setCancelled(true);
					player.sendMessage(
							Punisher.getInstance().getPunishmentManager().generatePunishmentMessage(punishment).get(0));
					return;
				}
				punishment = Punisher.getInstance().getPunishmentManager().getMostRecentSoftMute(player.getUniqueId());
				if (punishment != null) {
					Punisher.getInstance().getLogger().info("Silencing softmuted message from " + player.getName());
					List<Player> recipients = new LinkedList<>();
					recipients.add(player);
					for (Player p : Sponge.getServer().getOnlinePlayers()) {
						if (p.hasPermission("punisher.receive_notifications") && !recipients.contains(p)) {
							recipients.add(p);
						}
					}
					event.setChannel(new FixedMessageChannel(recipients));
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Listener(order = Order.LAST)
	public void punishMute(final SendCommandEvent event, @Root Player player) {
		Optional<? extends CommandMapping> command = Sponge.getCommandManager().get("me");
		if (!command.isPresent()) {
			return;
		}
		try {
			for (String alias : command.get().getAllAliases()) {
				if (alias.equalsIgnoreCase(event.getCommand())) {
					Punishment punishment =
							Punisher.getInstance().getPunishmentManager().getMostRecentMute(player.getUniqueId());
					if (punishment != null) {
						event.setCancelled(true);
						player.sendMessage(Punisher.getInstance()
								.getPunishmentManager()
								.generatePunishmentMessage(punishment)
								.get(0));
					}
					break;
				}
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Listener(order = Order.FIRST, beforeModifications = true)
	public void punishFreeze(final MoveEntityEvent event, @Root Player player) {
		Transform<World> from = event.getFromTransform();
		Transform<World> to = event.getToTransform();
		if (from.getExtent().equals(to.getExtent()) && from.getPosition().distanceSquared(to.getPosition()) == 0) {
			return;
		}
		if (PunisherDataSource.isConnected()) {
			if (!Punisher.getInstance()
					.getPunishmentManager()
					.getTemporaryExemptions()
					.contains(player.getUniqueId())) {
				try {
					Punishment punishment =
							Punisher.getInstance().getPunishmentManager().getMostRecentJail(player.getUniqueId());
					if (punishment == null) {
						punishment =
								Punisher.getInstance().getPunishmentManager().getMostRecentFreeze(player.getUniqueId());
					}
					if (punishment != null) {
						Punisher.getInstance()
								.getPunishmentManager()
								.getTemporaryExemptions()
								.add(player.getUniqueId());
						player.setTransform(event.getFromTransform());
						Punisher.getInstance()
								.getPunishmentManager()
								.getTemporaryExemptions()
								.remove(player.getUniqueId());
						player.sendMessage(Punisher.getInstance()
								.getPunishmentManager()
								.generatePunishmentMessage(punishment)
								.get(0));
					}
				}
				catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	@Listener(order = Order.FIRST, beforeModifications = true)
	public void punishFeeze(InteractEvent event, @Root Player player) {
		if (PunisherDataSource.isConnected()) {
			if (!Punisher.getInstance()
					.getPunishmentManager()
					.getTemporaryExemptions()
					.contains(player.getUniqueId())) {
				try {
					Punishment punishment =
							Punisher.getInstance().getPunishmentManager().getMostRecentFreeze(player.getUniqueId());
					if (punishment == null) {
						punishment =
								Punisher.getInstance().getPunishmentManager().getMostRecentJail(player.getUniqueId());
					}
					if (punishment != null) {
						event.setCancelled(true);
						player.sendMessage(Punisher.getInstance()
								.getPunishmentManager()
								.generatePunishmentMessage(punishment)
								.get(0));
					}
				}
				catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	@Listener(order = Order.FIRST, beforeModifications = true)
	public void punishFreeze(final InteractInventoryEvent event, @Root Player player) {
		if (PunisherDataSource.isConnected()) {
			if (!Punisher.getInstance()
					.getPunishmentManager()
					.getTemporaryExemptions()
					.contains(player.getUniqueId())) {
				try {
					Punishment punishment =
							Punisher.getInstance().getPunishmentManager().getMostRecentFreeze(player.getUniqueId());
					if (punishment == null) {
						punishment =
								Punisher.getInstance().getPunishmentManager().getMostRecentJail(player.getUniqueId());
					}
					if (punishment != null) {
						event.setCancelled(true);
						player.sendMessage(Punisher.getInstance()
								.getPunishmentManager()
								.generatePunishmentMessage(punishment)
								.get(0));
					}
				}
				catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	@Listener(order = Order.LAST, beforeModifications = true)
	public void punishJail(ClientConnectionEvent.Login event) {
		if (PunisherDataSource.isConnected()) {
			try {
				Punishment punishment = Punisher.getInstance()
						.getPunishmentManager()
						.getMostRecentJail(event.getProfile().getUniqueId());
				if (punishment != null) {
					WorldVector location = Punisher.getInstance().getPunisherConfig().getJailLocation();
					if (location != null) {
						event.setToTransform(new Transform<>(
								Sponge.getServer().getWorld(location.world).get(),
								new Vector3d(location.x, location.y, location.z),
								new Vector3d(location.pitch, location.yaw, 0.0)
						));
					}
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
