package dev.shortcircuit908.punisher.bukkit;

import com.google.common.collect.Lists;
import dev.shortcircuit908.punisher.bukkit.commands.AcknowledgeCommand;
import dev.shortcircuit908.punisher.bukkit.commands.PunishmentCommand;
import dev.shortcircuit908.punisher.bukkit.commands.ReloadCommand;
import dev.shortcircuit908.punisher.bukkit.commands.SetJailCommand;
import dev.shortcircuit908.punisher.bukkit.listeners.PlayerListener;
import dev.shortcircuit908.punisher.bukkit.listeners.PunishmentListener;
import dev.shortcircuit908.punisher.bukkit.punishment.PunishmentManagerBukkit;
import dev.shortcircuit908.punisher.common.IPunisherConfig;
import dev.shortcircuit908.punisher.common.IPunisherPlugin;
import dev.shortcircuit908.punisher.common.PunisherDataSource;
import dev.shortcircuit908.punisher.common.utils.WorldVector;
import com.zaxxer.hikari.pool.HikariPool;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * @author ShortCircuit908
 * Created on 3/19/2018.
 */
public class Punisher extends JavaPlugin implements IPunisherPlugin {
	private PunishmentManagerBukkit punishment_manager;
	
	@Override
	public void onLoad() {
	
	}
	
	@Override
	public void onEnable() {
		saveDefaultConfig();
		final PunishmentCommand punishment_command = new PunishmentCommand(this);
		getDescription().getCommands().entrySet().forEach((command_entry) -> {
			Map<String, Object> properties = command_entry.getValue();
			if ((boolean) properties.getOrDefault("punishment", false)) {
				PluginCommand command = getCommand(command_entry.getKey());
				command.setExecutor(punishment_command);
				command.setTabCompleter(punishment_command);
			}
		});
		
		getCommand("setjail").setExecutor(new SetJailCommand(this));
		getCommand("acknowledge").setExecutor(new AcknowledgeCommand(this));
		getCommand("reloadpunisher").setExecutor(new ReloadCommand(this));
		
		punishment_manager = new PunishmentManagerBukkit(this);
		
		getServer().getPluginManager().registerEvents(new PunishmentListener(this), this);
		getServer().getPluginManager().registerEvents(new PlayerListener(this), this);
		
		reload();
	}
	
	public void reload() {
		reloadConfig();
		Config config = getPunisherConfig();
		config.getIgnoreKickReasons();
		config.getServer();
		try {
			PunisherDataSource.tryConnect(config.getHost(), config.getPort(), config.getName(), config.getUsername(),
					config.getPassword());
			PunisherDataSource.executeScriptFromResource("/schema_blank.sql");
		}
		catch (HikariPool.PoolInitializationException e) {
			getLogger().severe(e.getMessage());
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		if (config.hasChanged()) {
			saveConfig();
		}
		punishment_manager.clearCache();
	}
	
	@Override
	public void onDisable() {
	
	}
	
	public PunishmentManagerBukkit getPunishmentManager() {
		return punishment_manager;
	}
	
	@Override
	public Config getPunisherConfig() {
		return new Config(getConfig());
	}
	
	public class Config implements IPunisherConfig {
		private final Configuration config;
		private boolean has_changed = false;
		
		private Config(Configuration config) {
			this.config = config;
		}
		
		public boolean hasChanged() {
			return has_changed;
		}
		
		public String getName() {
			if (!config.contains("database.name")) {
				has_changed = true;
				config.set("database.name", "");
			}
			return config.getString("database.name");
		}
		
		public String getHost() {
			if (!config.contains("database.host")) {
				has_changed = true;
				config.set("database.host", "localhost");
			}
			return config.getString("database.host");
		}
		
		public short getPort() {
			if (!config.contains("database.port")) {
				has_changed = true;
				config.set("database.port", 3306);
			}
			return (short) config.getInt("database.port");
		}
		
		public String getUsername() {
			if (!config.contains("database.username")) {
				has_changed = true;
				config.set("database.username", "root");
			}
			return config.getString("database.username");
		}
		
		public String getPassword() {
			if (!config.contains("database.password")) {
				has_changed = true;
				config.set("database.password", "");
			}
			return config.getString("database.password");
		}
		
		public WorldVector getJailLocation() {
			ConfigurationSection section = config.getConfigurationSection("jail");
			if (section == null) {
				return null;
			}
			if (!(section.contains("x") && section.contains("y") && section.contains("z") &&
					section.contains("world"))) {
				return null;
			}
			String world_name = section.getString("world");
			double x = section.getDouble("x");
			double y = section.getDouble("y");
			double z = section.getDouble("z");
			double yaw = section.getDouble("yaw", 0.0);
			double pitch = section.getDouble("pitch", 0.0);
			return new WorldVector(world_name, x, y, z, (float) yaw, (float) pitch);
		}
		
		public void setJailLocation(WorldVector location) {
			ConfigurationSection section = config.getConfigurationSection("jail");
			if (section == null) {
				section = config.createSection("jail");
			}
			section.set("world", location.getWorldName());
			section.set("x", location.x);
			section.set("y", location.y);
			section.set("z", location.z);
			section.set("yaw", location.yaw);
			section.set("pitch", location.pitch);
			has_changed = true;
		}
		
		public List<String> getIgnoreKickReasons() {
			if (!config.isList("ignore-kick-reasons")) {
				config.set("ignore-kick-reasons", Lists.newArrayList("server is .*", ".*\\b(afk|idle|idling)\\b.*"));
				has_changed = true;
			}
			return config.getStringList("ignore-kick-reasons");
		}
		
		public String getServer() {
			if (!config.contains("server-name")) {
				has_changed = true;
				config.set("server-name", "My Minecraft Server");
			}
			String server = config.getString("server-name");
			if (server.length() > 32) {
				server = server.substring(0, 32);
			}
			return server;
		}
	}
}
