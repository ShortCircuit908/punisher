package dev.shortcircuit908.punisher.bukkit.listeners;

import dev.shortcircuit908.punisher.bukkit.Punisher;
import dev.shortcircuit908.punisher.common.PunisherDataSource;
import dev.shortcircuit908.punisher.common.punishment.Punishment;
import dev.shortcircuit908.punisher.common.utils.StoredPlayerUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.server.ServerListPingEvent;

import java.sql.SQLException;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author ShortCircuit908
 * Created on 3/19/2018.
 */
@SuppressWarnings("unused")
public class PlayerListener implements Listener {
	private final Punisher plugin;
	
	public PlayerListener(Punisher plugin) {
		this.plugin = plugin;
	}
	
	// TODO: Address book and UUID map
	@EventHandler(priority = EventPriority.LOWEST)
	public void updatePlayerAddressBook(final AsyncPlayerPreLoginEvent event) {
		StoredPlayerUtils.PlayerInfo player =
				StoredPlayerUtils.getOrCreatePlayer(event.getUniqueId());
		player.addAddress(event.getAddress());
		try {
			plugin.getPunishmentManager().updateFirstJoinSinceIssued(player, Instant.now());
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@EventHandler
	public void updateLastOnline(final PlayerQuitEvent event) {
		StoredPlayerUtils.PlayerInfo player =
				StoredPlayerUtils.getOrCreatePlayer(event.getPlayer().getUniqueId());
		player.setUsername(event.getPlayer().getName());
		player.setLastOnline(Instant.now());
	}
	
	@EventHandler
	public void updateLastOnline(final PlayerKickEvent event) {
		StoredPlayerUtils.PlayerInfo player =
				StoredPlayerUtils.getOrCreatePlayer(event.getPlayer().getUniqueId());
		player.setUsername(event.getPlayer().getName());
		player.setLastOnline(Instant.now());
	}
	
	// TODO: Handle acknowledgements
	@EventHandler(priority = EventPriority.MONITOR)
	public void showAcknowledgements(final PlayerJoinEvent event) {
		final Player player = event.getPlayer();
		Bukkit.getServer().getScheduler().scheduleAsyncDelayedTask(plugin, () -> {
			if (PunisherDataSource.isConnected() && player.isOnline()) {
				try {
					List<Punishment> punishments =
							plugin.getPunishmentManager().getAllPunishments(player.getUniqueId());
					Optional<Long> row_id;
					for (Punishment punishment : punishments) {
						if (!punishment.isAcknowledged() && (row_id = punishment.getRowId()).isPresent()) {
							player.sendMessage(ChatColor.GOLD + "You were recently " + plugin.getPunishmentManager()
									.generatePunishmentMessage(punishment).get(2).replace('\n', ' ') +
									ChatColor.GOLD + ". You must acknowledge this action by typing " + ChatColor.RED +
									"/acknowledge " + row_id.get());
						}
					}
				}
				catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}, 20 * 10);
	}
	
	// TODO: IP Ban
	@EventHandler(priority = EventPriority.HIGHEST)
	public void punishIpBan(final AsyncPlayerPreLoginEvent event) {
		if (PunisherDataSource.isConnected()) {
			try {
				List<UUID> matching_ids = StoredPlayerUtils.getExistingPlayer(event.getAddress()).stream()
						.map(StoredPlayerUtils.PlayerInfo::getUniqueId)
						.filter(Objects::nonNull)
						.collect(Collectors.toList());
				for (UUID uuid : matching_ids) {
					Punishment ban = plugin.getPunishmentManager().getMostRecentIpBan(uuid);
					if (ban != null) {
						event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_BANNED, plugin.getPunishmentManager()
								.generatePunishmentMessage(ban).get(0).replace('\n', ' '));
						return;
					}
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	// TODO: Ban
	@EventHandler(priority = EventPriority.HIGHEST)
	public void punishBan(final AsyncPlayerPreLoginEvent event) {
		if (PunisherDataSource.isConnected()) {
			try {
				Punishment ban = plugin.getPunishmentManager().getMostRecentBan(event.getUniqueId());
				if (ban != null) {
					event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_BANNED, plugin.getPunishmentManager()
							.generatePunishmentMessage(ban).get(0).replace('\n', ' '));
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void punishBan(final ServerListPingEvent event) {
		if (PunisherDataSource.isConnected()) {
			try {
				List<UUID> matching_ids = StoredPlayerUtils.getExistingPlayer(event.getAddress()).stream()
						.map(StoredPlayerUtils.PlayerInfo::getUniqueId)
						.filter(Objects::nonNull)
						.collect(Collectors.toList());
				for (UUID uuid : matching_ids) {
					Punishment ban = plugin.getPunishmentManager().getMostRecentBan(uuid);
					if (ban == null) {
						ban = plugin.getPunishmentManager().getMostRecentIpBan(uuid);
					}
					if (ban != null) {
						event.setMotd(plugin.getPunishmentManager().generatePunishmentMessage(ban).get(0).replace
								('\n', ' '));
						return;
					}
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	// TODO: Mute
	@EventHandler(priority = EventPriority.HIGHEST)
	public void punishMute(final AsyncPlayerChatEvent event) {
		if (PunisherDataSource.isConnected()) {
			try {
				Punishment punishment =
						plugin.getPunishmentManager().getMostRecentMute(event.getPlayer().getUniqueId());
				if (punishment != null) {
					event.setCancelled(true);
					event.getPlayer().sendMessage(plugin.getPunishmentManager().generatePunishmentMessage(punishment)
							.get(0).replace('\n', ' '));
					return;
				}
				punishment = plugin.getPunishmentManager().getMostRecentSoftMute(event.getPlayer().getUniqueId());
				if (punishment != null) {
					plugin.getLogger().info("Silencing softmuted message from " + event.getPlayer().getName());
					try {
						List<Player> recipients = new LinkedList<>();
						recipients.add(event.getPlayer());
						for (Player player : Bukkit.getOnlinePlayers()) {
							if (player.hasPermission("punisher.receive_notifications") &&
									!recipients.contains(player)) {
								recipients.add(player);
							}
						}
						event.getRecipients().clear();
						event.getRecipients().addAll(recipients);
					}
					catch (UnsupportedOperationException e) {
						event.setCancelled(true);
					}
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void punishMute(final PlayerCommandPreprocessEvent event) {
		Command command = Bukkit.getServer().getPluginCommand("me");
		String command_string = event.getMessage().split("\\s")[0].substring(1);
		if (command != null) {
			boolean is_me = false;
			if (command_string.equalsIgnoreCase(command.getName())) {
				is_me = true;
			}
			if (!is_me) {
				for (String alias : command.getAliases()) {
					if (alias.equalsIgnoreCase(command_string)) {
						is_me = true;
						break;
					}
				}
			}
			if (is_me) {
				try {
					Punishment punishment =
							plugin.getPunishmentManager().getMostRecentMute(event.getPlayer().getUniqueId());
					if (punishment != null) {
						event.setCancelled(true);
						event.getPlayer()
								.sendMessage(plugin.getPunishmentManager().generatePunishmentMessage(punishment)
										.get(0).replace('\n', ' '));
					}
				}
				catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	// TODO: Freeze and Jail
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void punishFreeze(final PlayerMoveEvent event) {
		Location from = event.getFrom();
		Location to = event.getTo();
		if (to == null || from.getBlockX() != to.getBlockX() || from.getBlockY() != to.getBlockY() ||
				from.getBlockZ() != to.getBlockZ() ||
				!event.getFrom().getWorld().getUID().equals(event.getTo().getWorld().getUID())) {
			cancelMove(event);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void punishFreeze(final PlayerTeleportEvent event) {
		Location from = event.getFrom();
		Location to = event.getTo();
		if (to == null || from.getBlockX() != to.getBlockX() || from.getBlockY() != to.getBlockY() ||
				from.getBlockZ() != to.getBlockZ()
				|| !event.getFrom().getWorld().getUID().equals(event.getTo().getWorld().getUID())) {
			cancelFreeze(event);
			if (!event.isCancelled()) {
				cancelJail(event);
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void punishFreeze(final PlayerInteractEvent event) {
		cancelFreeze(event);
		if (!event.isCancelled()) {
			cancelJail(event);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void punishFreeze(final PlayerDropItemEvent event) {
		cancelFreeze(event);
		if (!event.isCancelled()) {
			cancelJail(event);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void punishFreeze(final PlayerPickupItemEvent event) {
		cancelFreeze(event);
		if (!event.isCancelled()) {
			cancelJail(event);
		}
	}
	
	public void cancelMove(final PlayerMoveEvent event) {
		if (PunisherDataSource.isConnected()) {
			try {
				Punishment punishment =
						plugin.getPunishmentManager().getMostRecentFreeze(event.getPlayer().getUniqueId());
				if (punishment != null) {
					plugin.getPunishmentManager().getTemporaryExemptions().add(event.getPlayer().getUniqueId());
					event.getPlayer().teleport(event.getFrom());
					plugin.getPunishmentManager().getTemporaryExemptions().remove(event.getPlayer().getUniqueId());
					event.getPlayer().sendMessage(plugin.getPunishmentManager().generatePunishmentMessage(punishment)
							.get(0).replace('\n', ' '));
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public <T extends PlayerEvent & Cancellable> void cancelFreeze(T event) {
		if (PunisherDataSource.isConnected()) {
			try {
				Punishment punishment =
						plugin.getPunishmentManager().getMostRecentFreeze(event.getPlayer().getUniqueId());
				if (punishment != null) {
					event.setCancelled(true);
					event.getPlayer().sendMessage(plugin.getPunishmentManager().generatePunishmentMessage(punishment)
							.get(0).replace('\n', ' '));
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public <T extends PlayerEvent & Cancellable> void cancelJail(T event) {
		if (PunisherDataSource.isConnected()) {
			if (!plugin.getPunishmentManager().getTemporaryExemptions().contains(event.getPlayer().getUniqueId())) {
				try {
					Punishment punishment =
							plugin.getPunishmentManager().getMostRecentJail(event.getPlayer().getUniqueId());
					if (punishment != null) {
						event.setCancelled(true);
						event.getPlayer()
								.sendMessage(plugin.getPunishmentManager().generatePunishmentMessage(punishment)
										.get(0).replace('\n', ' '));
					}
				}
				catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void punishJail(final PlayerJoinEvent event) {
		if (PunisherDataSource.isConnected()) {
			try {
				Punishment punishment =
						plugin.getPunishmentManager().getMostRecentJail(event.getPlayer().getUniqueId());
				if (punishment != null) {
					plugin.getPunishmentManager().applyJail(event.getPlayer());
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
