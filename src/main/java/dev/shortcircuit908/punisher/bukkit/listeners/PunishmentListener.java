package dev.shortcircuit908.punisher.bukkit.listeners;

import dev.shortcircuit908.punisher.bukkit.Punisher;
import dev.shortcircuit908.punisher.bukkit.punishment.PunishEvent;
import dev.shortcircuit908.punisher.common.punishment.Punishment;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.List;
import java.util.Optional;

/**
 * @author ShortCircuit908
 * Created on 3/20/2018.
 */
public class PunishmentListener implements Listener {
	private final Punisher plugin;
	
	public PunishmentListener(Punisher plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void cancelNoJail(final PunishEvent event) {
		if (event.getAction().equals(Punishment.Action.JAIL) && plugin.getPunisherConfig().getJailLocation() == null) {
			if (event.isIssuedByConsole()) {
				Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "The jail location has not been " +
						"set");
			}
			else {
				Player player = event.getIssuer().get().getPlayer();
				if (player != null) {
					player.sendMessage(
							ChatColor.RED + "The jail location has not been set. Set it with " + ChatColor.GOLD +
									"/setjail");
				}
			}
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void cancelShutdownKick(final PunishEvent event) {
		if (event.getAction().equals(Punishment.Action.KICK) && event.getReason().isPresent()) {
			List<String> matchers = plugin.getPunisherConfig().getIgnoreKickReasons();
			for (String matcher : matchers) {
				if (event.getReason().get().toLowerCase().matches(matcher.toLowerCase())) {
					event.setPersistent(false);
					plugin.getLogger().info("Setting suspected auto-kick to non-persistent");
					return;
				}
			}
		}
	}
	
	@EventHandler
	public void ensureOnline(final PunishEvent event) {
		if (event.getAction().equals(Punishment.Action.KICK) && !event.getTarget().isOnline()) {
			event.setCancelled(true);
			Optional<OfflinePlayer> punisher = event.getIssuer();
			if (punisher.isPresent()) {
				if (punisher.get().isOnline()) {
					punisher.get().getPlayer().sendMessage(ChatColor.RED + "Cannot kick " + ChatColor.GOLD
							+ event.getTarget().getName() + ChatColor.RED + ": Player is not online");
				}
			}
			else {
				Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "Cannot kick " + ChatColor.GOLD
						+ event.getTarget().getName() + ChatColor.RED + ": Player is not online");
			}
		}
	}
}
