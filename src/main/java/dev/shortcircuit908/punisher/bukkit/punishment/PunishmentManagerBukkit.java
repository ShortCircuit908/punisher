package dev.shortcircuit908.punisher.bukkit.punishment;

import dev.shortcircuit908.punisher.bukkit.Punisher;
import dev.shortcircuit908.punisher.common.punishment.Punishment;
import dev.shortcircuit908.punisher.common.punishment.PunishmentManagerBase;
import dev.shortcircuit908.punisher.common.utils.DurationUtils;
import dev.shortcircuit908.punisher.common.utils.StoredPlayerUtils;
import dev.shortcircuit908.punisher.common.utils.Tuple;
import dev.shortcircuit908.punisher.common.utils.WorldVector;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;

/**
 * @author ShortCircuit908
 * Created on 3/19/2018.
 */
public class PunishmentManagerBukkit extends PunishmentManagerBase {
	private final Punisher plugin_inst;
	
	public PunishmentManagerBukkit(Punisher plugin) {
		super(() -> plugin);
		this.plugin_inst = plugin;
	}
	
	public void addPunishment(Punishment punishment) throws SQLException {
		PunishEvent event = new PunishEvent(punishment);
		plugin_inst.getServer().getPluginManager().callEvent(event);
		if (event.isCancelled()) {
			return;
		}
		if (event.isPersistent()) {
			super.addPunishment(punishment);
			plugin_inst.getLogger().info("Logged punishment: " + punishment);
		}
		else {
			plugin_inst.getLogger().info("Handled non-persistent punishment: " + punishment);
			plugin_inst.getLogger().info("The punishment will not be logged and will not reflect in-game");
		}
		punishment_cache.remove(punishment.getTargetId());
		OfflinePlayer offline_target = Bukkit.getServer().getOfflinePlayer(punishment.getTargetId());
		Player target = offline_target.getPlayer();
		Tuple<String> messages = generatePunishmentMessage(punishment);
		String self_reason = messages.get(0);
		String other_reason = messages.get(1);
		if (target != null) {
			switch (punishment.getAction()) {
				case SOFTMUTE:
					break;
				case BAN:
				case IP_BAN:
				case KICK:
					target.kickPlayer(self_reason);
					break;
				case JAIL:
					applyJail(target);
					target.sendMessage(self_reason.replace("\n", " "));
					break;
				case UNJAIL:
					target.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
				default:
					target.sendMessage(self_reason.replace("\n", " "));
					break;
			}
		}
		Bukkit.getServer().broadcast(other_reason.replace("\n", " "), "punisher.receive_notifications");
	}
	
	public void applyJail(Player player) {
		WorldVector location = plugin_inst.getPunisherConfig().getJailLocation();
		if (location != null) {
			temporary_exemptions.add(player.getUniqueId());
			player.teleport(
					new Location(Bukkit.getServer().getWorld(location.world), location.x, location.y, location.z,
							location.yaw, location.pitch));
			temporary_exemptions.remove(player.getUniqueId());
		}
	}
	
	public Tuple<String> generatePunishmentMessage(Punishment punishment) {
		Optional<UUID> issuer_id = punishment.getIssuerId();
		String issuer_name = Bukkit.getServer().getConsoleSender().getName();
		String target_name = "{unknown}";
		try {
			Optional<StoredPlayerUtils.PlayerInfo> info = StoredPlayerUtils.getExistingPlayer(punishment.getTargetId());
			if (info.isPresent()) {
				target_name = info.get().getUsername().orElse("{unknown}");
			}
			else {
				throw new SQLException();
			}
		}
		catch (SQLException e) {
			OfflinePlayer target = Bukkit.getServer().getPlayer(punishment.getTargetId());
			if (target == null) {
				target = Bukkit.getServer().getOfflinePlayer(punishment.getTargetId());
			}
			if (target != null) {
				target_name = target.getName();
			}
		}
		if (issuer_id.isPresent()) {
			try {
				Optional<StoredPlayerUtils.PlayerInfo> info = StoredPlayerUtils.getExistingPlayer(issuer_id.get());
				if (info.isPresent()) {
					issuer_name = info.get().getUsername().orElse("{unknown}");
				}
				else {
					throw new SQLException();
				}
			}
			catch (SQLException e) {
				// Do nothing
				OfflinePlayer issuer = Bukkit.getServer().getPlayer(issuer_id.get());
				if (issuer == null) {
					issuer = Bukkit.getServer().getOfflinePlayer(issuer_id.get());
				}
				if (issuer != null) {
					issuer_name = issuer.getName();
				}
			}
		}
		StringBuilder reason_builder = new StringBuilder(ChatColor.GOLD.toString())
				.append(ChatColor.RED)
				.append(punishment.getAction().getAction())
				.append(ChatColor.GOLD)
				.append(" by ")
				.append(ChatColor.RED)
				.append(issuer_name);
		if (punishment.getAction().canExpire() && punishment.getTimeLeft().isPresent()) {
			reason_builder.append(ChatColor.GOLD)
					.append(" for ")
					.append(ChatColor.RED)
					.append(DurationUtils.toString(punishment.getTimeLeft().get()));
		}
		if (punishment.getReason().isPresent()) {
			reason_builder.append(ChatColor.GOLD)
					.append(":\n")
					.append(ChatColor.RED)
					.append(punishment.getReason().get());
		}
		String self_reason = ChatColor.GOLD + "You have been " + reason_builder.toString();
		String other_reason = ChatColor.RED + target_name + ChatColor.GOLD + " has been " + reason_builder.toString();
		return new Tuple<>(self_reason, other_reason, reason_builder.toString());
	}
}
