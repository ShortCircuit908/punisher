package dev.shortcircuit908.punisher.bukkit.punishment;

import dev.shortcircuit908.punisher.common.punishment.Punishment;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.time.Duration;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

/**
 * @author ShortCircuit908
 * Created on 3/20/2018.
 */
public class PunishEvent extends Event implements Cancellable {
	private static final HandlerList handlers = new HandlerList();
	private final Punishment punishment;
	private boolean cancelled = false;
	private boolean persistent = true;
	
	public PunishEvent(Punishment punishment) {
		this.punishment = punishment;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
	public Punishment.Action getAction() {
		return punishment.getAction();
	}
	
	public Optional<String> getReason() {
		return punishment.getReason();
	}
	
	public OfflinePlayer getTarget() {
		return Bukkit.getServer().getOfflinePlayer(punishment.getTargetId());
	}
	
	public Optional<OfflinePlayer> getIssuer() {
		Optional<UUID> uuid_opt = punishment.getIssuerId();
		return uuid_opt.map(uuid -> Bukkit.getServer().getOfflinePlayer(uuid));
	}
	
	public Optional<Duration> getDuration() {
		return punishment.getDuration();
	}
	
	public Date getDateIssued() {
		return punishment.getDateIssued();
	}
	
	public Optional<String> getServer() {
		return punishment.getServer();
	}
	
	public boolean isIssuedByConsole() {
		return !punishment.getIssuerId().isPresent();
	}
	
	public Punishment getPunishment() {
		return punishment;
	}
	
	public boolean isPersistent() {
		return persistent;
	}
	
	public void setPersistent(boolean persist) {
		this.persistent = persist;
	}
	
	@Override
	public boolean isCancelled() {
		return cancelled;
	}
	
	@Override
	public void setCancelled(boolean cancel) {
		this.cancelled = cancel;
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
}
