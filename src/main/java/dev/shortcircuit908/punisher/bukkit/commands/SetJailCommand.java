package dev.shortcircuit908.punisher.bukkit.commands;

import dev.shortcircuit908.punisher.bukkit.Punisher;
import dev.shortcircuit908.punisher.common.utils.WorldVector;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author ShortCircuit908
 * Created on 3/19/2018.
 */
public class SetJailCommand implements CommandExecutor {
	private final Punisher plugin;
	
	public SetJailCommand(Punisher plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Location location;
		if (sender instanceof Player) {
			location = ((Player) sender).getLocation();
		}
		else if (sender instanceof BlockCommandSender) {
			location = ((BlockCommandSender) sender).getBlock().getLocation();
		}
		else {
			sender.sendMessage(ChatColor.RED + "This command must be used ingame");
			return true;
		}
		plugin.getPunisherConfig()
				.setJailLocation(new WorldVector(location.getWorld().getName(), location.getX(), location.getY(),
						location.getZ(), location.getPitch(), location.getYaw()));
		plugin.saveConfig();
		sender.sendMessage(ChatColor.GOLD + "Set jail location to your position");
		return true;
	}
}
