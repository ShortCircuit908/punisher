package dev.shortcircuit908.punisher.bukkit.commands;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import dev.shortcircuit908.punisher.bukkit.Punisher;
import dev.shortcircuit908.punisher.common.punishment.Punishment;
import dev.shortcircuit908.punisher.common.utils.DurationUtils;
import dev.shortcircuit908.punisher.common.utils.StoredPlayerUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import java.sql.SQLException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author ShortCircuit908
 * Created on 3/19/2018.
 */
public class PunishmentCommand implements CommandExecutor, TabCompleter {
	private final Punisher plugin;
	
	public PunishmentCommand(Punisher plugin) {
		this.plugin = plugin;
	}
	
	private boolean process(CommandSender sender, Command command, String label, String[] args)
			throws CommandException {
		if (args.length < 1) {
			return false;
		}
		List<String> args_list = new LinkedList<>(Arrays.asList(args));
		// Get action
		String action_name = command.getLabel();
		Punishment.Action action = null;
		outer_loop:
		for (Punishment.Action check : Punishment.Action.values()) {
			for (String alias : check.getAliases()) {
				if (alias.equalsIgnoreCase(action_name)) {
					action = check;
					break outer_loop;
				}
			}
		}
		if (action == null) {
			throw new CommandException("Unknown action: " + action_name);
		}
		// Get target
		String target_name = args_list.remove(0);
		StoredPlayerUtils.PlayerInfo target = null;
		try {
			List<StoredPlayerUtils.PlayerInfo> targets = StoredPlayerUtils.getExistingPlayer(target_name);
			if (targets.isEmpty()) {
				throw new CommandException("Unknown player: " + target_name);
			}
			if (targets.size() > 1) {
				boolean exact_match = false;
				for (StoredPlayerUtils.PlayerInfo potential_target : targets) {
					if (potential_target.getUsername().orElse("{unknown}").equalsIgnoreCase(target_name)) {
						target = potential_target;
						exact_match = true;
						break;
					}
				}
				if (!exact_match) {
					List<String> names = new ArrayList<>(targets.size());
					for (StoredPlayerUtils.PlayerInfo check : targets) {
						names.add(check.getUsername().orElse("{unknown}"));
					}
					throw new CommandException("Ambiguous player names: " + Joiner.on(", ").join(names));
				}
			}
			else {
				target = targets.get(0);
			}
			// Get duration if present
			Duration duration = null;
			if (!args_list.isEmpty()) {
				String temp_duration = args_list.get(0);
				if (temp_duration.startsWith("@")) {
					args_list.remove(0);
					try {
						duration = DurationUtils.fromString(temp_duration.substring(1));
					}
					catch (IllegalArgumentException e) {
						throw new CommandException(e.getMessage());
					}
				}
			}
			boolean is_global = false;
			if (!args_list.isEmpty() && args_list.get(args_list.size() - 1).equalsIgnoreCase("--global")) {
				args_list.remove(args_list.size() - 1);
				is_global = true;
			}
			// Remaining args as reason
			String reason = Joiner.on(' ').join(args_list);
			if (reason.trim().isEmpty()) {
				reason = null;
			}
			Punishment punishment =
					new Punishment(-1, target.getUniqueId(), is_global ? null : plugin.getPunisherConfig().getServer(),
							sender instanceof OfflinePlayer ? ((OfflinePlayer) sender).getUniqueId() : null, action,
							reason,
							new Date(), duration, false,
							Bukkit.getServer().getOfflinePlayer(target.getUniqueId()).isOnline() ? new Date() : null);
			plugin.getPunishmentManager().addPunishment(punishment);
		}
		catch (SQLException e) {
			e.printStackTrace();
			throw new CommandException("An error has occurred", e);
		}
		return true;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		try {
			return process(sender, command, label, args);
		}
		catch (CommandException e) {
			sender.sendMessage(ChatColor.RED + e.getMessage());
		}
		return true;
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		if (args.length > 0) {
			return StoredPlayerUtils.getExistingPlayer(args[0]).stream()
					.map(info -> info.getUsername().orElse("{unknown}")).collect(Collectors.toList());
		}
		return ImmutableList.of();
	}
}
