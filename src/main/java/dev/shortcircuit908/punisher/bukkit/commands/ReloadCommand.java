package dev.shortcircuit908.punisher.bukkit.commands;

import dev.shortcircuit908.punisher.bukkit.Punisher;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * @author ShortCircuit908
 * Created on 3/19/2018.
 */
public class ReloadCommand implements CommandExecutor {
	private final Punisher plugin;
	
	public ReloadCommand(Punisher plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		plugin.reload();
		sender.sendMessage(ChatColor.GOLD + "Punisher reloaded!");
		return true;
	}
}
