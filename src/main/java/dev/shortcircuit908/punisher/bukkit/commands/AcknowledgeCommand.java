package dev.shortcircuit908.punisher.bukkit.commands;

import dev.shortcircuit908.punisher.bukkit.Punisher;
import dev.shortcircuit908.punisher.common.punishment.Punishment;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * @author ShortCircuit908
 * Created on 3/28/2018.
 */
public class AcknowledgeCommand implements CommandExecutor {
	private final Punisher plugin;
	
	public AcknowledgeCommand(Punisher plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof OfflinePlayer)) {
			sender.sendMessage(ChatColor.RED + "This command is player-only");
			return true;
		}
		if (args.length == 0) {
			return false;
		}
		long id;
		try {
			id = Long.parseLong(args[0]);
		}
		catch (NumberFormatException e) {
			sender.sendMessage(ChatColor.RED + e.getMessage());
			return true;
		}
		try {
			List<Punishment> punishments =
					plugin.getPunishmentManager().getAllPunishments(((OfflinePlayer) sender).getUniqueId());
			Optional<Long> row_id;
			for (Punishment punishment : punishments) {
				row_id = punishment.getRowId();
				if (row_id.isPresent() && row_id.get() == id) {
					if (punishment.isAcknowledged()) {
						sender.sendMessage(ChatColor.GOLD + "You have already acknowledged this action");
					}
					else {
						plugin.getPunishmentManager().acknowledgePunishment(id);
						sender.sendMessage(ChatColor.GOLD + "Action acknowledged");
					}
					return true;
				}
			}
		}
		catch (SQLException e) {
			throw new CommandException("An error has occurred", e);
		}
		sender.sendMessage(ChatColor.GOLD + "No action has been found with ID " + ChatColor.RED + id);
		return true;
	}
}
