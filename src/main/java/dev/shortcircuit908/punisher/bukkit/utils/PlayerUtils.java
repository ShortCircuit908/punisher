package dev.shortcircuit908.punisher.bukkit.utils;

import com.google.common.collect.Lists;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.Server;
import org.bukkit.entity.Player;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author ShortCircuit908
 * Created on 3/19/2018.
 */
public class PlayerUtils {
	public static List<Player> getOnlinePlayers(final String name) {
		List<Player> players = getOnlinePlayers();
		players.removeIf(player -> !player.getName().toLowerCase().contains(name.toLowerCase())
				&& !player.getDisplayName().toLowerCase().contains(name.toLowerCase()));
		return players;
	}
	
	public static List<OfflinePlayer> getPlayers(String name) {
		List<OfflinePlayer> players = Lists.newArrayList(Bukkit.getServer().getOfflinePlayers());
		players.removeIf(player -> !player.getName().toLowerCase().contains(name.toLowerCase())
				&&
				!(player.isOnline() && player.getPlayer().getDisplayName().toLowerCase().contains(name.toLowerCase())));
		return players;
	}
	
	public static List<OfflinePlayer> getPlayersFavoringOnline(String name) {
		List<Player> online_players = getOnlinePlayers(name);
		if (online_players.size() == 1) {
			return new ArrayList<>(online_players);
		}
		List<OfflinePlayer> offline_players = getPlayers(name);
		offline_players.removeIf(player -> {
			for (Player check : online_players) {
				if (check.getUniqueId().equals(player.getUniqueId())) {
					return true;
				}
			}
			return false;
		});
		List<OfflinePlayer> values = new ArrayList<>(online_players.size() + offline_players.size());
		values.addAll(online_players);
		values.addAll(offline_players);
		return values;
	}
	
	@SuppressWarnings("unchecked")
	public static List<Player> getOnlinePlayers() {
		try {
			Method method = Server.class.getDeclaredMethod("getOnlinePlayers");
			Object value = method.invoke(Bukkit.getServer());
			if (value instanceof Player[]) {
				return Lists.newArrayList((Player[]) value);
			}
			return new ArrayList<>((Collection<? extends Player>) value);
		}
		catch (ReflectiveOperationException e) {
			e.printStackTrace();
		}
		return new ArrayList<>(0);
	}
}
