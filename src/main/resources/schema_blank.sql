CREATE TABLE IF NOT EXISTS `punisher_uuid_map`
(
	`id`          INTEGER AUTO_INCREMENT,
	`player_id`   CHARACTER(36),
	`username`    VARCHAR(16) NOT NULL,
	`last_online` DATETIME DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY (`player_id`)
);

CREATE TABLE IF NOT EXISTS `punisher_ip_address_book`
(
	`id`        INTEGER AUTO_INCREMENT,
	`player_id` INTEGER       NOT NULL,
	`address`   CHARACTER(45) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY (`player_id`, `address`),
	FOREIGN KEY (`player_id`) REFERENCES `punisher_uuid_map` (`id`) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `punisher_punishments`
(
	`id`                     INTEGER AUTO_INCREMENT,
	`target_id`              INTEGER     NOT NULL,
	`server`                 VARCHAR(32) NOT NULL,
	`issuer_id`              CHARACTER(36)        DEFAULT NULL,
	`action`                 VARCHAR(16) NOT NULL,
	`reason`                 TEXT                 DEFAULT NULL,
	`issued`                 DATETIME    NOT NULL DEFAULT NOW(),
	`duration`               VARCHAR(32)          DEFAULT NULL,
	`acknowledged`           TINYINT(1)  NOT NULL DEFAULT 0,
	`first_join_since_issue` DATETIME             DEFAULT NULL,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`target_id`) REFERENCES `punisher_uuid_map` (`id`) ON DELETE CASCADE
);
